<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $message;
     public $code; 
     public $publisher;
     public $email;
    public function __construct($message,$code,$publisher,$email)
    {
        //return Route::current()->parameters();
        $this->message=$message;
        $this->code= $code;
        $this->publisher= $publisher;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = array();
        $data[0] = $this->publisher;
        $data[1]= $this->email;
        $data[2] = $this->message;
        $data[3] = $this->code;
        return $this->from('Quiziu@Site.com','Quiziu')->view('mail',compact('data'));
    }
}
