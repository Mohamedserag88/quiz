<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rate;
use App\User;
use App\Models\Exam;
use App\Http\Controllers\Controller;
use Flash;
use LaravelLocalization;
class RatingController extends Controller
{
    public function rate(Request $request){
        $input = $request->all();

        $model = $input['exam'];
        $module=  Exam::find($input['exam']);
        $input['rateable_type']='App\Models\Exam';
        $input['rateable_id']=$module->id;
        $input['rater_id']=\Auth::user()->id;
        $ratebefor=Rate::where('rateable_type', $input['rateable_type'])
                        ->where('rateable_id',$input['rateable_id'])
                        ->where('rater_id',$input['rater_id'])->first();
        if($ratebefor){
            $ratebefor->rate=$input['rate'];
            $ratebefor->comment=$input['comment'];
            $ratebefor->save();
        } 
        else{
            Rate::create($input);
            
        }   
        Flash::success(__('website.ratedone'));          
      return redirect(LaravelLocalization::getCurrentLocale().'/');

    }

    public function rateForm($exam_id){
        return view('rate',compact('exam_id'));
    }
}
