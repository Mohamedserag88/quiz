<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Http\Controllers\API\BaseController as Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Student;
use App\Models\ExamPublisher;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $usertype=\Session::get('type');
        if($usertype && $usertype=='exampublisher'){
            $validator= Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'policyaccept'=>'required'
            ]);
        }
        else{
            $validator= Validator::make($data, [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'policyaccept'=>'required'
            ]);
        }
        return $validator;
        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $validator=$this->validator($data);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $usertype=\Session::get('type');
        if($usertype && $usertype=='exampublisher'){
            $user= User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'type'=>'exampublisher'
            ]);
            $result=ExamPublisher::create([
                'user_id'=>$user->id,
                'company_name'=>$data['company_name'],
                'company_industry'=>$data['company_industry']
            ]);
        }
        else{
            $user= User::create([
                'name' => $data['first_name'].' '.$data['last_name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'type'=>'student'
            ]);
            
            $result=Student::create([
                'user_id'=>$user->id,
                'first_name'=> $data['first_name'],
                'last_name'=> $data['last_name']
                ]);

        }
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;


        return $this->sendResponse($success, 'User register successfully.');
        
    }
}
