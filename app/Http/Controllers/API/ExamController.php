<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Type;
use App\Http\Controllers\API\BaseController as Controller;
use App\Models\Exam;
use App\Models\RemainAnswer;
use App\Models\Answer;
use LaravelLocalization;
use Flash;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $exams = Exam::all();


        return $this->sendResponse($exams->toArray(), 'Exams retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories=Category::all();
        return view('exams.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required',
            'conditions'=>'required',
            'language'=>'required',
            'category_id'=>'required',
            'tags_skills'=>'required',
            'number_of_students'=>'required',
            'code'=>'required',
            'price'=>'required'
            
        ]);
        $input =$request->all();
        $input['user_id']=\Auth::user()->id;
        $input['total_points']=0;
        $exam=Exam::create($input);
        return redirect(LaravelLocalization::getCurrentLocale().'/questions/create?exam='.$exam->id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function conditions($id){
        $exam=Exam::find($id);
        return view('exams.conditions',compact('exam'));
    }
    public function code($id){
        $exam=Exam::find($id);
        return view('exams.code',compact('exam'));
    }
    
    public function codepage(){
        return view('exams.codepage');
    }
    public function exampre(Request $request){
        $input=$request->all();
        $exam=Exam::find($input['exam_id']);
        if($exam->code == $input['code']){
            return view('exams.exam',compact('exam'));
        }
        else{
            Flash::error(__('website.codeerror'));
            return back();
        }
    }
    public function examprepage(Request $request){
        $input=$request->all();
        $exam=Exam::where('code',$input['code'])->first();

        if($exam && $exam->number_of_students > $exam->passed_students){
            $exam->passed_students+=1;
            $exam->save();
            return view('exams.exam',compact('exam'));
        }
        else{
            Flash::error(__('website.codeerror'));
            return back();
        }
    }
    public function correct(Request $request){
        $input=$request->all();
        $exam=Exam::find($input['exam_id']);
        $points=0;
        foreach($exam->questions as $question){
            if(isset($input['ans_'.$question->id])){
                if($question->type->id == 1){
                
                    $ans=Answer::find($input['ans_'.$question->id]);
                    if($ans){
                        if($ans->correct_answer){
                            $points+=$question->points;
                        }
                    }
                    else{
                        $points+=$question->points;
                    }
                    
                }
                elseif($question->type->id == 2){
                    $anspoints=0;
                    foreach($input['ans_'.$question->id] as $studentans){
                        $ans=Answer::find($studentans);
                        if($ans){
                            if($ans->correct_answer){
                                $anspoints+=ceil(($question->points)/count($question->answers));
                            }
                        } 
                        else{
                            $points+=$question->points;
                        }
                    }
                    $points+=($anspoints > $question->points)?$question->points:$anspoints;
                }
                elseif($question->type->id == 4){
                    $ans=$question->answers->first();
                    if($ans){
                        if(strpos($ans->answers, $input['ans_'.$question->id]) === TRUE){
                            $points+=$question->points;
                        }
                    }
                    else{
                        $points+=$question->points;
                    }
                    
                }
                else{
                    RemainAnswer::create([
                        'question_id'=>$question->id,
                        'answer'=>$input['ans_'.$question->id],
                        'user_id'=>\Auth::user()->id,
                        'corrected'=>0
                    ]);
                }
            }
            
        }
        $examcheck=\Auth::user()->exampassed->where('exam_id',$exam->id)->first();
        if($examcheck){
            \Auth::user()->exampassed()->detach($exam->id);
            \Auth::user()->exampassed()->attach($exam->id, ['points' => $points]);
        }
        else{
            \Auth::user()->exampassed()->attach($exam->id, ['points' => $points]);
        }
        
        redirect(LaravelLocalization::getCurrentLocale().'/exam/result?exam='.$exam->id);
        
        
    }
    public function examresult(){
         
         $pre= [
            'url'=> \URL::to('/').'/'.LaravelLocalization::getCurrentLocale()
         ];
         \Session::put('_previous',$pre);
        
        $exam=Exam::find($_GET['exam']);
        if(!$exam){
            return view('errors.404');
            // Flash::error(__('website.pagenotfound'));
            // return redirect(LaravelLocalization::getCurrentLocale().'/');
        }
        $examcheck=\Auth::user()->exampassed->where('id',$exam->id)->first();
        if(!$examcheck){
            return view('errors.404');
            // Flash::error(__('website.pagenotfound'));
            // return redirect(LaravelLocalization::getCurrentLocale().'/');
        }
        $points=$examcheck->pivot->points;
        $mesaage=__('website.exammessage');
        return view('exams.result',compact('mesaage','points','exam'));
    }
}
