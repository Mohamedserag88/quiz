<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exam;
use App\Models\Type;
use App\Models\File;
use App\Models\Answer;
use App\Models\Question;
use Flash;
use LaravelLocalization;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (isset($_GET['exam'])) {
            $exam = Exam::find($_GET['exam']);
            $types = Type::all();
            return view('questions.create', compact('exam', 'types'));
        }
        Flash::error('Page Not Found');
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'type_id' => 'required',
            'question' => 'required',
            'points' => 'required'
        ]);
        $input = $request->all();
        unset($input['answers'][0]);
        unset($input['correct'][0]);
        if ($input['type_id'] == 1 || $input['type_id'] == 2) {
            if (count($input['answers']) == 0) {
                Flash::error('Please Enter a Answers For The Question');
                return back();
            } else {
                if ($request->file('photo')) {
                    $file = $request->file('photo');
                    $fileid = File::upload([$file], 'uploads');
                    $input['photo'] = '/' . File::where('id', $fileid[0])->get()->first()->path;
                } else {
                    $input['photo'] = NULL;
                }
                $question = Question::create([
                    'type_id' => $input['type_id'],
                    'exam_id' => $input['exam_id'],
                    'question' => $input['question'],
                    'hint' => $input['hint'],
                    'points' => $input['points'],
                    'photo' => $input['photo']
                ]);
                for ($i = 1; $i < count($input['answers']) + 1; $i++) {
                    Answer::create([
                        'question_id' => $question->id,
                        'answers' => $input['answers'][$i],
                        'correct_answer' => $input['correct'][$i]
                    ]);
                }
            }
        } else {
            if ($request->file('photo')) {
                $file = $request->file('photo');
                $fileid = File::upload([$file], 'uploads');
                $input['photo'] = '/' . File::where('id', $fileid[0])->get()->first()->path;
            } else {
                $input['photo'] = NULL;
            }
            $question = Question::create([
                'type_id' => $input['type_id'],
                'exam_id' => $input['exam_id'],
                'question' => $input['question'],
                'hint' => $input['hint'],
                'points' => $input['points'],
                'photo' => $input['photo']
            ]);
            for ($i = 1; $i < count($input['answers']) + 1; $i++) {
                Answer::create([
                    'question_id' => $question->id,
                    'answers' => $input['answers'][$i],
                    'correct_answer' => $input['correct'][$i]
                ]);
            }
        }
        $exam = Exam::find($input['exam_id']);
        $exam->total_points = $exam->questions->sum('points');
        $exam->save();
        Flash::success('Question Has been Successfully Added');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $question = Question::find($id);
        $types = Type::all();
        if ($question) {
            return view('questions.edit', compact('question', 'types'));
        } else {
            return view('errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'type_id' => 'required',
            'question' => 'required',
            'points' => 'required'
        ]);
        $input = $request->all();
        unset($input['_method']);
        unset($input['_token']);
        unset($input['save_button']);
        $answers = $input['answers'];
        $corrects = $input['correct'];
        unset($input['answers']);
        unset($input['correct']);
        $question = Question::find($id);
        if (!$question) {
            Flash::error('Question not found');
            return redirect(LaravelLocalization::getCurrentLocale() . '/questions/create?exam=' . $question->exam->id);
        }
        $question = Question::where('id', $id)->update($input);
        $question = Question::find($id);
        foreach ($question->answers as $ans) {
            $ans->delete();
        }
        unset($answers[0]);
        unset($corrects[0]);
        for ($i = 1; $i < count($answers) + 1; $i++) {
            Answer::create([
                'question_id' => $question->id,
                'answers' => $answers[$i],
                'correct_answer' => $corrects[$i]
            ]);
        }

        return redirect(LaravelLocalization::getCurrentLocale() . '/questions/create?exam=' . $question->exam->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question = Question::find($id);
        $question->exam;
        if (empty($question) || $question->exam->user_id != \Auth::id()) {
            Flash::error('Question not found');

            return back();
        }

        $question->delete();

        Flash::success('Question deleted successfully.');

        return back();
    }
}
