<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\ExamPublisher;
use App\User;
use App\Models\Exam;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topcategories=Category::all()->sortByDESC('topcategory')->chunk(4);
        $toppublisher=ExamPublisher::all()->sortByDESC('topexamer')->take(6);
        $exams=Exam::all()->take(8);
        return view('home',compact('topcategories','toppublisher','exams'));
    }
    public function Search(Request $request){
        $query=$request->get('search');
        if($request->has('search') && $query!=''){
            $users = User::search($query)->where('type','exampublisher')->get();
            $exams = Exam::search($query)->get();
            $categories=Category::search($query)->get();
    	}else{
    		$users = User::where('type','exampublisher')->get();
    		$exams = Exam::all();
    		$categories = Category::all();
        }
        return view('search',compact('query','users','exams','categories'));

    }
}
