<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Code;
use LaravelLocalization;

class CodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $exam_id=$_GET['exam'];
        return view('codes.create',compact('exam_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'code'=>'required|array',
            'exam_id'=>'required|integer',
            'type'=>'required|array',
            'student_number'=>'required|array'

            
        ]);
        $input=$request->all();
        for($i=0;$i<count($input['type']);$i++){
            Code::create([
                'exam_id'=>$input['exam_id'],
                'code'=>$input['code'][$i],
                'type'=>$input['type'][$i],
                'student_number'=>$input['student_number'][$i],
                'used'=>0
            ]);
        }
        return redirect(LaravelLocalization::getCurrentLocale().'/profile');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
