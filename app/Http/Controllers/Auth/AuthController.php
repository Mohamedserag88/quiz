<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Student;
use App\Models\ExamPublisher;

class AuthController extends Controller
{
    
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);

        return redirect('/');
    }

   
    public function findOrCreateUser($user, $provider)
    {
        if($provider == 'twitter')
            $authUser = User::where('email', $user->nickname)->first();
        else
            $authUser = User::where('email', $user->email)->first();

        if ($authUser) {
            return $authUser;
        }
        $usertype=\Session::get('type');
        $user = User::create([
            'name'     => $user->name,
            'email'    => ($provider == 'twitter') ? $user->nickname :$user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'password' => bcrypt('123456'),
            'type'=>($usertype)?$usertype:'student'
        ]);
        $this->DetaileRecord($user);
        $user->provider = $provider;
        return $user;
    }
    public function DetaileRecord($user){
        if($user->type == 'student')
        {   
            $result=Student::create([
                'user_id'=>$user->id,
                'first_name'=>$user->name,
                'last_name'=>''
                ]);
        }
        else
        {   
            $result=ExamPublisher::create([
                'user_id'=>$user->id,
                'company_name'=>'',
                'company_industry'=>''
            ]);
        }
        return $result;
    }
}