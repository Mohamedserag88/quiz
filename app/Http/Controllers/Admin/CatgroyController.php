<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Controllers\Controller;
use LaravelLocalization;
use Flash;

class CatgroyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories=Category::all();
        return view('admin.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required',
            'slug'=>'required|unique:categories'
        ]);
        $input=$request->all();
        Category::create($input);
        return redirect(LaravelLocalization::getCurrentLocale().'/admin/category');
    }

   
    public function edit($id)
    {
        //
        $category=Category::find($id);
        if (empty($category) ) {
            Flash::error('category not found');

            return back();
        }
        return view('admin.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name'=>'required'
        ]);
        $input=$request->all();
        unset($input['_method']);
        unset($input['_token']);
        $category=Category::where('id', $id)->update($input);
        return redirect(LaravelLocalization::getCurrentLocale().'/admin/category');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);

        if (empty($category) ) {
            Flash::error('category not found');

            return back();
        }

        $category->delete();

        Flash::success('category deleted successfully.');

        return back();
    }
    
}
