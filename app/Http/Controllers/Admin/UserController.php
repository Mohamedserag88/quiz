<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Flash;
use App\User;
use App\Models\Follow;
use App\Http\Controllers\Controller;
use App\DataTables\UsersDatatable;

class UserController extends Controller
{
    
    public function index(UsersDatatable $admin)
    {
        //
        if(isset($_GET['type']))
            $users=User::where('type',$_GET['type'])->get();
        else
            $users= User::all();
        return view('admin.users.index',compact('users'));
         

    }

    public function show($id)
    {
        //
        $user=User::find($id);
        if($user){
            if($user->type=='student'){
                return view('student.show',compact('user'));
            }
            else{
                return view('exampublisher.show',compact('user'));
            }

        }
        else{
            Flash::error('User not found');

            return back();
        }
    }
    public function profile(){
        $user=\Auth::user();
        if(\Auth::user()->type=='student'){
            return view('student.profile',compact('user'));
        }
        else{
            return view('exampublisher.profile',compact('user'));
        }
    }
    public function editprofile(){
        $user=\Auth::user();
        if(\Auth::user()->type=='student'){
            return view('student.edit',compact('user'));
        }
        else{
            return view('exampublisher.edit',compact('user'));
        }
    }

    public function follow($following,$follower){
        $follow=Follow::where([
            'follower_id'=>$follower,
            'following_id'=>$following
        ])->first();
        if($follow){
            $follow->forceDelete();
            return "Follow";
        }
        else{
            Follow::create([
                'follower_id'=>$follower,
                'following_id'=>$following
            ]);
            return "Following";
        }
    }
}
