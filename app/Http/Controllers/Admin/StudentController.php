<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\File;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email|unique:users,email,'.\Auth::user()->id,
            'phone'=>'required'
            
        ]);
        $input = $request->all();
        $user=\Auth::user();
        $user->name=$input['first_name'].' '.$input['last_name'];
        $user->email=$input['email'];
        if($input['password']){
            $user->password=bcrypt($input['password']);
        }
        if ($request->file('profile')) {
            $file = $request->file('profile');
           $fileid=File::upload([$file],'uploads');
           $user->profile='/'.File::where('id',$fileid[0])->get()->first()->path;

        }
        if ($request->file('cover')) {
            $file = $request->file('cover');
           $fileid=File::upload([$file],'uploads');
           $user->cover='/'.File::where('id',$fileid[0])->get()->first()->path;

        }
        $user->save();
        $student=$user->student;
        $student->f_name=$input['first_name'];
        $student->l_name=$input['last_name'];
        $student->phone=$input['phone'];
        $student->pio=$input['pio'];
        $student->profile_language=$input['profile_language'];
        $student->save();
        return redirect(LaravelLocalization::getCurrentLocale().'/profile');
    }

}
