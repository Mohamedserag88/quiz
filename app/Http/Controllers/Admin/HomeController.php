<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\ExamPublisher;
use App\User;
use App\Models\Exam;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.home');
    }
    public function Search(Request $request){
        $query=$request->get('search');
        if($request->has('search') && $query!=''){
            $users = User::search($query)->where('type','exampublisher')->get();
            $exams = Exam::search($query)->get();
            $categories=Category::search($query)->get();
    	}else{
    		$users = User::where('type','exampublisher')->get();
    		$exams = Exam::all();
    		$categories = Category::all();
        }
        return view('search',compact('query','users','exams','categories'));

    }
}
