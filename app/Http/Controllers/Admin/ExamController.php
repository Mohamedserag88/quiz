<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Type;
use App\Models\Exam;
use App\Models\RemainAnswer;
use App\Models\Answer;
use LaravelLocalization;
use Flash;
use Session;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $exams = Exam::all();
        return view('admin.exams.index',compact('exams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories=Category::all();
        return view('admin.exams.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required',
            'conditions'=>'required',
            'language'=>'required',
            'category_id'=>'required',
            'timer'=>'required'
        ]);
        $input =$request->all();
        $input['user_id']=\Auth::user()->id;
        $input['total_points']=0;
        $input['code']=$this->codgen(20);
        $exam=Exam::create($input);
        return redirect(LaravelLocalization::getCurrentLocale().'/admin/exams');
        
    }
    public function edit($id)
    {
        //
        $exam=Exam::find($id);
        if (empty($exam)  ) {
            Flash::error('Exam not found');

            return back();
        }
        $categories=Category::all();
        return view('admin.exams.edit',compact('exam','categories'));
        
    }
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name'=>'required',
            'conditions'=>'required',
            'language'=>'required',
            'category_id'=>'required',
            'tags_skills'=>'required',
            'timer'=>'required'
            
        ]);
        $input = $request->all();
        unset($input['_method']);
        unset($input['_token']);
        $exam=Exam::find($id);
        if(!$exam){
            Flash::error('Exam not found');

            return back();
        }
        
        $exam=Exam::where('id', $id)->update($input);
        return redirect(LaravelLocalization::getCurrentLocale().'/admin/exams');
        
    }

    public function destroy($id)
    {
        //
        $exam = Exam::find($id);

        if (empty($exam)  ) {
            Flash::error('Exam not found');

            return back();
        }

        $exam->delete();

        Flash::success('Exam deleted successfully.');

        return redirect('admin/exams');
    }

    public function codgen($number){
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $number; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

}
