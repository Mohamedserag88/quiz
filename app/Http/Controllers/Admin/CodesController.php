<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Code;
use App\Models\Exam;
use LaravelLocalization;
use Flash;
use App\Http\Controllers\Controller;


class CodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $codes=Code::all();
        return view('admin.codes.index',compact('codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $exams=Exam::all();
        return view('admin.codes.create',compact('exams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'code'=>'required',
            'exam_id'=>'required|integer',
            'type'=>'required',
            'student_number'=>'required'

            
        ]);
        $input=$request->all();
        Code::create($input);
        
        return redirect(LaravelLocalization::getCurrentLocale().'/admin/codes');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $code=Code::find($id);
        if(!$code){
            Flash::error('Code not found');

            return back();
        }
        return view('admin.codes.edit',compact('code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'code'=>'required',
            'exam_id'=>'required|integer',
            'type'=>'required',
            'student_number'=>'required'

            
        ]);
        $input=$request->all();
        unset($input['_method']);
        unset($input['_token']);
        $code=code::find($id);
        if(!$code){
            Flash::error('Code not found');

            return back();
        }
        
        $code=code::where('id', $id)->update($input);
        return redirect(LaravelLocalization::getCurrentLocale().'/admin/codes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $code=Code::find($id);
        $code->delete();
        return redirect(LaravelLocalization::getCurrentLocale().'/admin/codes');

    }
}
