<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Type;
use App\Models\Exam;
use App\Models\RemainAnswer;
use App\Models\Answer;
use App\Models\Code;
use App\Models\User_Answer;
use App\User;
use LaravelLocalization;
use Flash;
use Session;
use App\Mail\sendmail;

class ExamController extends Controller
{

    public function create()
    {
        //
        $categories = Category::all();
        return view('exams.create', compact('categories'));
    }


    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'conditions' => 'required',
            'language' => 'required',
            'category_id' => 'required',
            'tags_skills' => 'required',
            'timer' => 'required'


        ]);
        $input = $request->all();
        $input['user_id'] = \Auth::user()->id;
        $input['total_points'] = 0;
        $input['code'] = $this->codgen(20);
        $input['draft_public'] = 0;
        $exam = Exam::create($input);
        return redirect(LaravelLocalization::getCurrentLocale() . '/questions/create?exam=' . $exam->id);
    }


    public function edit($code)
    {
        //
        $exam = Exam::where('code', $code)->first();
        $categories = Category::all();
        if ($exam) {
            return view('exams.edit', compact('exam', 'categories'));
        } else {
            return view('errors.404');
        }
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'conditions' => 'required',
            'language' => 'required',
            'category_id' => 'required',
            'tags_skills' => 'required',
            'timer' => 'required'
        ]);
        $exam = Exam::find($id);
        if (!$exam) {
            Flash::error('Exam not found');

            return redirect('profile');
        }
        $input = $request->only(['name', 'conditions', 'language', 'category_id', 'tags_skills', 'timer', 'price']);
        $exam = Exam::where('id', $id)->update($input);
        return redirect(LaravelLocalization::getCurrentLocale() . '/questions/create?exam=' . $id);
    }
    public function destroy($id)
    {
        //
        $exam = Exam::find($id);

        if (empty($exam) || $exam->user_id != \Auth::id()) {
            Flash::error('Exam not found');

            return redirect('profile');
        }

        $exam->delete();

        Flash::success('Exam deleted successfully.');

        return redirect('profile');
    }

    public function conditions($id)
    {
        $exam = Exam::find($id);
        if (empty($exam)) {

            Flash::error(__('website.codeerror'));
            return back();
        }
        return view('exams.conditions', compact('exam'));
    }
    public function code($id)
    {
        $exam = Exam::find($id);
        if (empty($exam)) {
            Flash::error(__('website.codeerror'));
            return back();
        }
        return view('exams.code', compact('exam'));
    }

    public function codepage()
    {
        return view('exams.codepage');
    }
    public function exampre(Request $request)
    {
        $input = $request->all();
        $exam = Exam::find($input['exam_id']);
        $code = $exam->codes->where('code', $input['code'])->first();
        if ($code && $exam && $code->student_number > $code->used) {
            if (Session::get('timer') == Null) {
                Session::put('timer', $exam->timer);
                Session::put('start_timer', date('y-m-d H:i:s'));
                Session::put('stop_timer', date('y-m-d H:i:s', strtotime('+' . $exam->timer . 'minutes', strtotime(Session::get('start_timer')))));
            }
            $code->used += 1;
            $code->save();
            return redirect('/examsolve/' . $exam->code);
        } else {
            Flash::error(__('website.codeerror'));
            return back();
        }
    }
    public function examprepage(Request $request)
    {
        $input = $request->all();
        $code = Code::where('code', $input['code'])->first();
        if(empty($code)) {
            Flash::error(__('website.codeerror'));
            return back();
        }
        $exam = $code->exam;
        if ($code && $exam && $code->student_number > $code->used) {
            if (Session::get('timer') == Null) {
                Session::put('timer', $exam->timer);
                Session::put('start_timer', date('y-m-d H:i:s'));
                Session::put('stop_timer', date('y-m-d H:i:s', strtotime('+' . $exam->timer . 'minutes', strtotime(Session::get('start_timer')))));
            }
            $code->used += 1;
            $code->save();
            return redirect('/examsolve/' . $exam->code);
        } else {
            Flash::error(__('website.codeerror'));
            return back();
        }
    }
    public function examsolve($code)
    {
        $exam = Exam::where('code', $code)->first();
        if (empty($exam)) {
            Flash::error(__('website.codeerror'));
            return back();
        }
        return view('exams.exam', compact('exam'));
    }
    public function correct(Request $request)
    {
        $input = $request->all();
        $exam = Exam::find($input['exam_id']);
        $points = 0;
        $corr = collect();
        foreach ($exam->questions as $question) {
            if (isset($input['ans_' . $question->id])) {
                if ($question->type->id == 1) {

                    $ans = Answer::find($input['ans_' . $question->id]);
                    if ($ans) {
                        User_Answer::create([
                            'exam_id' => $exam->id,
                            'user_id' => \Auth::user()->id,
                            'question_id' => $question->id,
                            'answer' => $ans->answers,
                        ]);
                        if ($ans->correct_answer) {
                            $points += $question->points;
                        }
                    } else {
                    }
                } elseif ($question->type->id == 2) {
                    $anspoints = 0;
                    foreach ($input['ans_' . $question->question] as $studentans) {
                        $ans = Answer::find($studentans);

                        if ($ans) {
                            if ($ans->correct_answer) {
                                $anspoints += ceil(($question->points) / count($question->answers));
                                User_Answer::create([
                                    'exam_id' => $exam->id,
                                    'user_id' => \Auth::user()->id,
                                    'question_id' => $question->id,
                                    'answer' => $ans->answers,
                                ]);
                            }
                        } else {
                        }
                    }
                    $points += ($anspoints > $question->points) ? $question->points : $anspoints;
                } elseif ($question->type->id == 4) {
                    $ans = $question->answers->first();
                    if ($ans) {
                        if ($ans->answers == $input['ans_' . $question->id]) {
                            $points += $question->points;
                            User_Answer::create([
                                'exam_id' => $exam->id,
                                'user_id' => \Auth::user()->id,
                                'question_id' => $question->id,
                                'answer' => $ans->answers,
                            ]);
                        }
                    } else {
                    }
                } else {
                    RemainAnswer::create([
                        'question_id' => $question->id,
                        'answer' => $input['ans_' . $question->id],
                        'user_id' => \Auth::user()->id,
                        'corrected' => 0
                    ]);
                    User_Answer::create([
                        'exam_id' => $exam->id,
                        'user_id' => \Auth::user()->id,
                        'question_id' => $question->id,
                        'answer' => $input['ans_' . $question->id],
                    ]);
                }
            }
        }
        $examcheck = \Auth::user()->exampassed->where('id', $exam->id)->first();
        if ($examcheck) {
            \Auth::user()->exampassed()->detach($exam->id);
            \Auth::user()->exampassed()->attach($exam->id, ['points' => $points]);
        } else {
            \Auth::user()->exampassed()->attach($exam->id, ['points' => $points]);
        }
        Session::forget('timer');
        Session::forget('start_timer');
        Session::forget('stop_timer');
        return redirect(LaravelLocalization::getCurrentLocale() . '/exam/result?exam=' . $exam->id);
    }
    public function examresult()
    {

        $pre = [
            'url' => \URL::to('/') . '/' . LaravelLocalization::getCurrentLocale()
        ];
        \Session::put('_previous', $pre);

        $exam = Exam::find($_GET['exam']);
        if (!$exam) {
            return view('errors.404');
        }
        $examcheck = \Auth::user()->exampassed->where('id', $exam->id)->first();
        if (!$examcheck) {
            return view('errors.404');
        }
        $points = $examcheck->pivot->points;
        $mesaage = __('website.exammessage');
        return view('exams.result', compact('mesaage', 'points', 'exam'));
    }


    public function codgen($number)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $number; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }
    public function publishexam($code)
    {
        $exam = Exam::where('code', $code)->first();
        $exam->draft_public = 1;
        $exam->save();
        return redirect(LaravelLocalization::getCurrentLocale() . '/codes/create?exam=' . $exam->id);
    }
    public function getexamcodes($code)
    {
        $exam = Exam::where('code', $code)->first();
        $students = User::where('type', 'student')->get();
        return view('codes.share', compact('exam', 'students'));
    }
    public function sharecode(Request $request)
    {
        $input = $request->all();
        $code = Code::find($input['code']);
        $student = User::find($input['user_id']);
        \Mail::to($student->email)->send(new sendmail('Quiziu Happy to Send You code For my Exam ', $code->code, \Auth::user()->name, \Auth::user()->email), function ($m) {
            $m->from('careerz.hut@gmail.com', 'Quiziu');
        });
        return back();
    }
}
