<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use LaravelLocalization;


class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email|unique:users,email,'.\Auth::user()->id,
            'phone'=>'required'
            
        ]);
        $input = $request->all();
        $user=\Auth::user();
        $user->name=$input['first_name'].' '.$input['last_name'];
        $user->email=$input['email'];
        if($input['password']){
            $user->password=bcrypt($input['password']);
        }
        if ($request->file('profile')) {
            $file = $request->file('profile');
           $fileid=File::upload([$file],'uploads');
           $user->profile='/'.File::where('id',$fileid[0])->get()->first()->path;

        }
        if ($request->file('cover')) {
            $file = $request->file('cover');
           $fileid=File::upload([$file],'uploads');
           $user->cover='/'.File::where('id',$fileid[0])->get()->first()->path;

        }
        $user->save();
        $student=$user->student;
        $student->first_name=$input['first_name'];
        $student->last_name=$input['last_name'];
        $student->phone=$input['phone'];
        $student->pio=$input['pio'];
        $student->profile_language=$input['profile_language'];
        $student->save();
        return redirect(LaravelLocalization::getCurrentLocale().'/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
