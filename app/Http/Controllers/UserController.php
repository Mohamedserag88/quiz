<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Flash;
use App\User;
use App\Models\Follow;

class UserController extends Controller
{

    public function show($id)
    {
        $user = User::query()->find($id);
        if ($user) {
            if ($user->type == 'student') {
                return view('student.show', compact('user'));
            } else {
                return view('exampublisher.show', compact('user'));
            }
        } else {
            Flash::error('User not found');
            return back();
        }
    }

    public function profile()
    {
        $user = \Auth::user();
        if (\Auth::user()->type == 'student') {
            return view('student.profile', compact('user'));
        } elseif (\Auth::user()->type == 'exampublisher') {
            return view('exampublisher.profile', compact('user'));
        } else {
            return back();
        }
    }
    public function editprofile()
    {
        $user = \Auth::user();
        if (\Auth::user()->type == 'student') {
            return view('student.edit', compact('user'));
        } 
        elseif (\Auth::user()->type == 'exampublisher') {
            return view('exampublisher.edit', compact('user'));
        } else {
            return back();
        }
    }

    public function follow($following, $follower)
    {
        $follow = Follow::where([
            'follower_id' => $follower,
            'following_id' => $following
        ])->first();
        if ($follow) {
            $follow->forceDelete();
            return "Follow";
        } else {
            Follow::create([
                'follower_id' => $follower,
                'following_id' => $following
            ]);
            return "Following";
        }
    }
}
