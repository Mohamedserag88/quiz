<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_Answer extends Model
{
    public $table = 'user_answers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'exam_id',
        'user_id',
        'question_id',
        'answer',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
       
    ];


    public function exam()
    {
        return $this->belongsTo(\App\Models\Exam::class);
    }
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
    public function question()
    {
        return $this->belongsTo(\App\Models\Question::class);
    }
}
