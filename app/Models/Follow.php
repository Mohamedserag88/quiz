<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    public $fillable = [
        'follower_id',
        'following_id'
    ];

    
    
}
