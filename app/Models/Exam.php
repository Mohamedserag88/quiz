<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Exam extends Model
{
    //
    use SearchableTrait;
    public $table = 'exams';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at','updated_at'];

    protected $searchable = [
        'columns' => [
            'exams.name' => 10,
            'exams.conditions'=>5,
            'exams.tags_skills'=>3
        ]
    ];
    public $fillable = [
        'name',
        'user_id',
        'conditions',
        'language',
        'category_id',
        'total_points',
        'tags_skills',
        'code',
        'draft_public',
        'image',
        'price',
        'timer'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
    public function students(){
        return $this->belongsToMany('App\User','user_exams')->withPivot('points');
    }
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }
    public function questions(){
        return $this->hasMany('App\Models\Question');
    }

    public function rates()
    {
        return $this->morphMany('App\Models\Rate', 'rateable');
    }

    protected $appends = array('ratesum','rateavg');

    public function getRateSumAttribute(){
         return $this->rates->sum('rate');

    }
    public function getRateAvgAttribute(){
        if(count($this->rates)!=0){
            $avg= $this->ratesum / count($this->rates);
        }
        else 
            $avg=0;

        return $avg;

    }

    public function codes(){
        return $this->hasMany('App\Models\Code');

    }
    
}
