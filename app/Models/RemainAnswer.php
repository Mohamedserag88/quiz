<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemainAnswer extends Model
{
    //
    public $table = 'remain_answers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'question_id',
        'answer',
        'user_id',
        'corrected'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
       
    ];


    public function question()
    {
        return $this->belongsTo(\App\Models\Question::class);
    }
    public function student()
    {
        return $this->belongsTo(\App\User::class);
    }
}
