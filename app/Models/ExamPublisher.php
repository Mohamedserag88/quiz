<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamPublisher extends Model
{
    //
    

    public $table = 'exam_publishers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'company_name',
        'company_industry',
        'phone',
        'pio',
        'profile_language',
        'website',
        'myjob'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];
    protected $appends = array('topexamer');
    public function user(){
        return $this->belongsTo('\App\User');
    }
    public function getTopexamerAttribute(){
        if(isset($this->user))
            return count($this->user->examposted);    
        else
            return 0;
    }
    
    // public function users()
    // {
    //     return $this->hasManyThrough('App\User', 'App\Models\Exam');
    // }
}
