<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
/**
 * Class Category
 * @package App\Models
 * @version October 18, 2016, 1:20 pm UTC
 */
class Category extends Model
{
    use SearchableTrait;
    public $table = 'categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $searchable = [
        'columns' => [
            'categories.name_en' => 10,
            'categories.name_ar' => 10
        ]
    ];
    public $fillable = [
        'name_en',
        'name_ar',
        'slug',
        'descrpition',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'descrpition' => 'text'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name_en'=>'required|min:2|unique:categories,name_en',
        'name_ar'=>'required|min:2,'
    ];

    protected $appends = array('topcategory','name');
    public function exams()
    {
        return $this->hasMany(\App\Models\Exam::class);
    }
    public function getTopcategoryAttribute(){
        return count($this->exams);
    }
    public function getNameAttribute(){
        $name = 'name_'.\App::getLocale();
        return $this->$name;
    }



}
