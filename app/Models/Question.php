<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    public $table = 'questions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'type_id',
        'exam_id',
        'question',
        'hint',
        'points',
        'photo',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function exam()
    {
        return $this->belongsTo(\App\Models\Exam::class);
    }
    public function type(){
        return $this->belongsTo(\App\Models\Type::class);
    }
    public function answers(){
        return $this->hasMany(\App\Models\Answer::class);
    }
}
