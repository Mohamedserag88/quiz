<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rate
 * @package App\Models
 * @version October 18, 2016, 1:23 pm UTC
 */
class Rate extends Model
{
    use SoftDeletes;

    public $table = 'rates';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'rate',
        'comment',
        'rater_id',
        'rateable_type' ,
        'rateable_id' ,
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rate' => 'integer',
        'comment' => 'string',
        'rater_id' => 'integer',
        'rateable_type' => 'string',
        'rateable_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function rateable()
    {
        return $this->morphTo();
    }
    public function user(){
        return $this->belongsTo('App\User','rater_id','id');
    }

}
