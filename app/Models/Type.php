<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PostType
 * @package App\Models
 * @version March 28, 2018, 8:31 pm UTC
 *
 * @property string name
 * @property string slug
 * @property integer parent_id
 * @property integer lft
 * @property integer rgt
 * @property integer depth
 */
class Type extends Model
{
    use SoftDeletes;

    public $table = 'types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name_en',
        'name_ar',
        'slug',
        'descrpition'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'descrpition' => 'text'
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    protected $appends = array('name');
    public function getNameAttribute(){
        $name = 'name_'.\App::getLocale();
        return $this->$name;
    }

    public function exams()
    {
        return $this->hasMany(\App\Models\Exam::class);
    }


    
}
