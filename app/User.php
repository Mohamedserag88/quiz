<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use SearchableTrait;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'users.name' => 10,
            'users.email' => 10
        ]
    ];
    protected $fillable = [
        'name', 'email', 'password','provider','provider_id','profile','cover','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function student(){
        return $this->hasOne('App\Models\Student');
    }
    public function exampublisher(){
        return $this->hasOne('App\Models\ExamPublisher');
    }
    public function examposted(){
        return $this->hasMany('App\Models\Exam');
    }
    public function exampassed(){
        return $this->belongsToMany('App\Models\Exam','user_exams')->withPivot('updated_at','points');
    }
    public function followers(){
        return $this->belongsToMany('App\User','follows','following_id','follower_id');
    }
    public function followings(){
        return $this->belongsToMany('App\User','follows','follower_id','following_id');
    }
    public function followcheck($id){
        $check=$this->followings->where('id',$id)->first();
        if($check){
            return true;
        }
        else{
            return false;
        }
    }
}
