<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => '  يجب ان يكون مقبول.',
    'active_url'           => '  عنوان غير صالح.',
    'after'                => ' :date يجب ان يكون بعد .',
    'after_or_equal'       => ' :date يجب ان يكون بعد او يساوى.',
    'alpha'                => '  يجب ان يحتوى على حروف فقط.',
    'alpha_dash'           => '  يمكن ان يحتوى على ارقام وحروف و _.',
    'alpha_num'            => '  يمكن ان يحتوى على حروف وارقام.',
    'array'                => '  يجب ان تكون مجموعه.',
    'before'               => ' :date يجب ان يكون قبل .',
    'before_or_equal'      => ' :date يجب ان يكون قبل او يساوى.',
    'between'              => [
        'numeric' => '  يجب ان يكون بين :min و :max.',
        'file'    => '  يجب ان يكون بين :min و :max كيلوبايت.',
        'string'  => '  يجب ان يكون بين :min و :max حرف.',
        'array'   => '  يجب ان يكون بين :min و :max عناصر.',
    ],
    'boolean'              => '  يجب ان يكون صح او خطأ.',
    'confirmed'            => ' التأكيد غير متطابق.',
    'date'                 => '  تاريخ غير صالح.',
    'date_format'          => '  لا تقابل الشكل التالى :format.',
    'different'            => '  و :other يجب ان يكونو مختلفين.',
    'digits'               => '  يجب ان يكون :digits ارقام.',
    'digits_between'       => '  يجب ان يكون بين :min و :max أرقام.',
    'dimensions'           => '  ابعاد الصورة غير صحيحه.',
    'distinct'             => '  يحتوى على بيانات مكررة.',
    'email'                => '  يجب ان يكون بريد الكترونى صحيح.',
    'exists'               => '   غير صالح.',
    'file'                 => '  يجب ان يكون ملف.',
    'filled'               => '  يجب ان يحتوى على بيانات.',
    'image'                => '  يجب ان يكون صورة',
    'in'                   => '   غير صالح.',
    'in_array'             => ' :other غير موجود ضمن هذه القيم .',
    'integer'              => '  يجب ان يكون عدد صحيح.',
    'ip'                   => '  عنوان Ip غير صالح.',
    'ipv4'                 => '  must be a valid IPv4 address.',
    'ipv6'                 => '  must be a valid IPv6 address.',
    'json'                 => '  must be a valid JSON string.',
    'max'                  => [
        'numeric' => '  may not be greater than :max.',
        'file'    => '  may not be greater than :max kilobytes.',
        'string'  => '  may not be greater than :max characters.',
        'array'   => '  may not have more than :max items.',
    ],
    'mimes'                => '  must be a file of type: :values.',
    'mimetypes'            => '  must be a file of type: :values.',
    'min'                  => [
        'numeric' => '  must be at least :min.',
        'file'    => '  must be at least :min kilobytes.',
        'string'  => '  must be at least :min characters.',
        'array'   => '  must have at least :min items.',
    ],
    'not_in'               => ' selected  is invalid.',
    'numeric'              => '  must be a number.',
    'present'              => '  field must be present.',
    'regex'                => '  format is invalid.',
    'required'             => ' الحقل  مطلوب',
    'required_if'          => '  field is required when :other is :value.',
    'required_unless'      => '  field is required unless :other is in :values.',
    'required_with'        => '  field is required when :values is present.',
    'required_with_all'    => '  field is required when :values is present.',
    'required_without'     => '  field is required when :values is not present.',
    'required_without_all' => '  field is required when none of :values are present.',
    'same'                 => '  and :other must match.',
    'size'                 => [
        'numeric' => '  must be :size.',
        'file'    => '  must be :size kilobytes.',
        'string'  => '  must be :size characters.',
        'array'   => '  must contain :size items.',
    ],
    'string'               => '  must be a string.',
    'timezone'             => '  must be a valid zone.',
    'unique'               => '  موجود بالفعل .',
    'uploaded'             => '  failed to upload.',
    'url'                  => '  format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
