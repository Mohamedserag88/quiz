<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
    </head>
    <body>
    <h1>This message form Queziu Auther {{ $data[0] }}:</h1>
      <h2>The email {!!($data[1]) !!}</h2>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                   With message  {!! ($data[2]) !!}
                </div>
                <div class="title m-b-md">
                        The Code For the Exam   {!! ($data[3]) !!}
                </div>

                
            </div>
        </div>
    </body>
</html>
