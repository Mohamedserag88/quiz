@extends('layouts.app')

@section('content')
<main style="display: flex">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-sm-10  m-auto pre-exam">
                @include('flash::message')
                <h3 name="code_h3">{{ __('website.codes') }}</h3>
                {!! Form::open(['route' => 'codes.store','method'=>'post']) !!}
                    <input type="hidden" name="exam_id" value="{{ $exam_id }}"> 
                    <div>
                        @if ($errors->has('code'))
                            <span class="invalid-feedback" style="display:block;">
                                <strong style="color:red;">{{ $errors->first('code') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('type'))
                            <span class="invalid-feedback" style="display:block;">
                                <strong style="color:red;">{{ $errors->first('type') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('student_number'))
                            <span class="invalid-feedback" style="display:block;">
                                <strong style="color:red;">{{ $errors->first('student_number') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="exam-code exam-padding">
                        <label>{{ __('website.gencode') }}</label>
                        <div class="exam-co row" style="margin-bottom:15px">
                            <div class="col-lg-5">
                                {!! Form::select('type',['Group'=>'Group','Student'=>'Student'], null, ['class' => 'form-control','id'=>'codetype','required'=>'required','placeholder'=> __('website.codetype')]) !!}
                            </div>
                            <div class="col-lg-5">
                                <input type="number" class="form-control" id="number_of_students_code" placeholder="{{ __('website.enterstudent') }}">
                            </div>
                            <div class="col-lg-2">
                                <button class="add-answer" style="min-width: 120px;" type="button" id="generatecode">{{ __('website.Generate') }}</button>
                            </div>
                        </div>
                    </div> 
                    <div class="row ">
                        <div class="col-lg-3">
                            <label name="code_type_label">{{ __('website.codetype') }}</label>
                        </div>
                        <div class="col-lg-3">
                            <label name="code_student_number_label">{{ __('website.studentnumber') }}</label>
                        </div>
                        <div class="col-lg-4">
                            <label name="code_code_label">{{ __('website.code') }}</label>
                        </div>
                    </div>
                    <div id="codesdiv">
                    </div> 
                    <div class="add-quiz-button">
                        <button name="code_submit_btn">{{ __('website.save') }}</button>
                    </div>                   
                </form>
            </div>
        </div>

    </div>
</main>
@endsection