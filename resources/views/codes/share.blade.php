
            
                @foreach ($exam->codes as $code)
                    <div class="row " style="margin-bottom: 15px;">
                        <div class="col-lg-3">
                            <input name="type" type="text" readonly class="form-control"   value="{{ $code->type }}">
                        </div>
                        <div class="col-lg-2">
                            <input name="student_number" type="text" readonly class="form-control"   value="{{ $code->student_number - $code->used }}">
                        </div>
                        <div class="col-lg-5">
                            <input name="" type="text" readonly class="form-control"   value="{{ $code->code }}">
                        </div>
                        @if($code->student_number == $code->used)
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-secondary btn-sm" style="width: 100%;height: 100%;">{{ __('website.closed') }}</a>
                            </div>
                            
                        @else
                            <div class="col-lg-2">
                                <a name="code_shar_btn" type="button" href="" data-toggle="modal" data-target="#usermodel"  class="btn btn-secondary btn-sm sharbtncode" code-id="{{ $code->id }}" style="width: 100%;height: 100%;">{{ __('website.share') }}</a>
                            </div>
                        @endif    
                    </div>
                @endforeach
               
            

<div class="modal fade" id="usermodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                        {!! Form::open(['route' => 'sharecode']) !!}
                        <input type="hidden" name="code" value="" id="codeidinput">
                            {!! Form::label('language', __('website.selectstudent') ) !!}                            
                            {!! Form::select('user_id',$students->pluck('name','id'),null,['class'=>'form-control']) !!}
                            <div class="row " style="margin-bottom: 15px;">
                                    <div class="col-lg-2">
                                        <button type="submit"  class="btn btn-secondary btn-sm" style="width: 100%;height: 100%;">{{ __('website.share') }}</button>
                                    </div>
                            </div>
                            {!! Form::close() !!}
                    
                </div>
                <div class="modal-footer">
                    <button name="code_user_btn" type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('website.close') }}</button>
                </div>
        </div>
    </div>
</div>