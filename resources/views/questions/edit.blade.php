@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-12 create-exam">
            @include('flash::message')
            {!! Form::model($question,['route' => ['questions.update', $question->id],'files'=>'true','method' => 'patch']) !!}
                <div class="quiz-container" >
                    <div class="title-quiz col-flex-quiz">
                        <div class="row w-100 m-0">
                            <div class="col-md-3 p-0">
                                <select class="form-control" name="type_id" id="question-type">
                                    <option selected disabled>{{ __('website.Questiontype') }}</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}" {{ ($question->type_id==$type->id)?'selected':'' }}>{{$type->name}} </option>
                                    @endforeach
                                    
                                </select>
                                @if ($errors->has('type_id'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;" name="type_id_strong">{{ $errors->first('type_id') }}</strong>
                                    </span>
                                @endif
                            </div><!--col-->
                            <div class="col-md-9">
                                <input type="text" name="question" value="{{ $question->question }}" placeholder="{{ __('website.typequestion') }}" class="form-control"/>
                                @if ($errors->has('question'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;" name="question_strong">{{ $errors->first('question') }}</strong>
                                    </span>
                                @endif
                            </div><!--col-->
                        </div><!--row-->
                       
                    </div>
                    <div class="container-exam">
                        <div class="hint-image-input">
                            <div class="col-md-4">
                                <div id="file-upload-form" class="uploader">
                                    <input id="file-upload" name="photo" type="file" name="fileUpload" accept="image/*" />
                                    <label for="file-upload" id="file-drag">
                                        <img id="file-image"  src="{{ asset($question->photo) }}" alt="Preview" >
                                        <div id="start" style="min-height: 50px;">
                                        
                                            <div>{{ __('website.AddImage') }}</div>
                                            <div id="notimage" class="hidden">{{ __('website.selectimg') }}</div>
                                        
                                        </div>
                                        <div id="response" class="hidden">
                                            <div id="messages"></div>
                                            <progress class="progress" id="file-progress" value="0">
                                                <span>0</span>%
                                            </progress>
                                        </div>
                                    </label>

                                </div>
                            </div><!--col-->
                            <div class="col-md-8">
                                <textarea placeholder="{{ __('website.hint') }}" name="hint"  value="{{ $question->hint }}" class="form-control"></textarea>
                            </div><!--col-->
                        </div><!--hint-image-input-->
                        <div class="row m-0 point-num">
                            <label>{{ __('website.Points') }}:</label><input min="1" name="points"  value="{{ $question->points }}" type="number" placeholder="{{ __('website.Points') }}" class="form-control" />
                            @if ($errors->has('points'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;" name="points_strong">{{ $errors->first('points') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="row m-0 answers-area">
                            <div class="ans-area col-flex-quiz">
                                <div class="row w-100 m-0">
                                    <input type="text" id="ansvalue" class="form-control" placeholder="{{ __('website.typeans') }}"/>
                                </div><!--row-->
                                <div class="row m-0 chec-ans">
                                    <div class="col p-0">
                                        <input type="checkbox" id="cor-1" />
                                        <label for="cor-1">{{ __('website.correctans') }}</label>
                                    </div>
                                    <div class="col ml-auto p-0">
                                        <button class="add-answer" id="addanswer">{{ __('website.AddAnswer') }}</button>
                                    </div>
                                </div><!--row-->
                            </div><!--ans-area-->
                            <input type="hidden" name="answers[]">
                            <input type="hidden" name="correct[]">
                            <div id="aswers-area">
                                @foreach ($question->answers as $answer)
                                    <div class="ans-divbody">
                                        <div class="">
                                            <button type="button" style="float: right;" class="btn btn-danger btn-smv removeansbtn">{{ __('website.removeAnswer') }}</button>
                                        </div>
                                        <div calss="ans-area">
                                                <input type="hidden" name="answers[]" value="{{ $answer->answers }}">
                                            @if($answer->correct_answer)
                                                <input type="hidden" name="correct[]" value="1">
                                                <input type="checkbox" checked disabled/>
                                            @else
                                                <input type="hidden" name="correct[]" value="0">
                                                <input type="checkbox" disabled/>
                                            @endif
                                            <div class="ans-text">
                                                {{ $answer->answers }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            
                            
                            <div class="save-quiz">
                                <button name="save_button">{{ __('website.save') }}</button>
                            </div>

                        </div>
                            
                    </div><!--container-exam-->	
                        
                </div>
            </form>
        </div>
    </div>
</div>

@endsection