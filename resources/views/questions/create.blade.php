@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-12 create-exam">
            @include('flash::message')
            <div class="clearfix" style="margin-top:20px">
                <h3 class="title-section pull-left" name="create_exam_a">{{ __('website.createquiz') }}</h3>
                <a class="add-answer pull-right text-center" name="exam_publish_a" style="min-width:120px" href="/{{ LaravelLocalization::getCurrentLocale() }}/publishexam/{{ $exam->code }}">{{ __('website.publish') }}</a>
            </div>
            @php
            $i=1;
            @endphp
            @foreach ($exam->questions as $question)
                <div class="quiz-container" id="questionbody">
                    <div class="title-quiz">
                        <span class="num">{{$i}}-  </span>
                        <span class="type-quiz" name="quiz_type_span">{{$question->type->name}}</span>
                        <span class="text-quiz" name="quiz_span">{{$question->question}}</span>
                        {!! Form::open(['route' => ['questions.destroy', $question->id], 'method' => 'delete','style'=>'margin-left: 245px;']) !!}
                                <a style="margin-right: 20px;" name="quiz_edit_a" href="/{{ LaravelLocalization::getCurrentLocale() }}/questions/{{$question->id}}/edit" class="edit-quiz ml-auto"><i class="fa fa-edit"></i> {{ __('website.Edit') }} </a>                                
                                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>'.__('website.delete'), ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        {!! Form::close() !!}
                                
                    </div>
                    <div class="container-exam">
                        <div class="row m-0 hint-image">
                            <div class="col-md-4 col image-quiz">
                                <a href="{{$question->photo}}" data-fancybox="gallery"><img src="{{asset($question->photo)}}" alt="" /></a>
                            </div><!--col-->
                            <div class="col-md-8 col hint-quiz">
                                <h4>{{ __('website.hint') }}</h4>
                                <article name="quiz_hint_artical">{{$question->hint}}</article>
    
                            </div><!--col-->
                        </div>
                        <div class="row m-0 point-num">
                            <label name="points_label">{{ __('website.Points') }}:</label> <span min="1" name="points_span">{{$question->points}}</span>
                        </div>
                        <div class="row m-0 answers-area">
                            @foreach($question->answers as $ans)
                                <div class="ans-area">
                                    @if($question->type_id == 1 || $question->type_id == 2)
                                        <input type="checkbox" disabled {{($ans->correct_answer)?'checked':''}} name="ans_checkbox"/>
                                    @endif
                                    <div class="ans-text">
                                        {{$ans->answers}}
                                    </div>
                                </div><!--ans-area-->
                            @endforeach
                        </div>							
                    </div><!--container-exam-->
                </div><!--quiz-container-->
                @php
                $i++;
                @endphp
            @endforeach
            
            {!! Form::open(['route' => 'questions.store','files'=>'true']) !!}
                <input type="hidden" name="exam_id" value="{{$exam->id}}">
                <div class="quiz-container" >
                    <div class="title-quiz col-flex-quiz">
                        <div class="row w-100 m-0">
                            <div class="col-md-3 p-0">
                                <select class="form-control" name="type_id" id="question-type">
                                    <option selected disabled>{{ __('website.Questiontype') }}</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}} </option>
                                    @endforeach
                                    
                                </select>
                                @if ($errors->has('type_id'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong name="type_id_strong" style="color:red;">{{ $errors->first('type_id') }}</strong>
                                    </span>
                                @endif
                            </div><!--col-->
                            <div class="col-md-9">
                                <input type="text" name="question" placeholder="{{ __('website.typequestion') }}" class="form-control"/>
                                @if ($errors->has('question'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;" name="question_strong">{{ $errors->first('question') }}</strong>
                                    </span>
                                @endif
                            </div><!--col-->
                        </div><!--row-->
                        <div class="collapse-div">
                            <i class="fas fa-angle-down"></i>
                        </div>
                    </div>
                    <div class="container-exam">
                        <div class="hint-image-input">
                            <div class="col-md-4">
                                <div id="file-upload-form" class="uploader">
                                    <input id="file-upload" name="photo" type="file" name="fileUpload" accept="image/*" />
                                    <label for="file-upload" id="file-drag">
                                        <img id="file-image"  src="#" alt="Preview" class="hidden">
                                        <div id="start">
                                        
                                            <div>{{ __('website.AddImage') }}<br/> 500 X 300</div>
                                            <div id="notimage" class="hidden">{{ __('website.selectimg') }}</div>
                                        
                                        </div>
                                        <div id="response" class="hidden">
                                            <div id="messages"></div>
                                            <progress class="progress" id="file-progress" value="0">
                                                <span>0</span>%
                                            </progress>
                                        </div>
                                    </label>

                                </div>
                            </div><!--col-->
                            <div class="col-md-8">
                                <textarea placeholder="{{ __('website.hint') }}" name="hint" class="form-control"></textarea>
                            </div><!--col-->
                        </div><!--hint-image-input-->
                        <div class="row m-0 point-num">
                            <label>{{ __('website.Points') }}:</label><input name="points" type="number" placeholder="{{ __('website.Points') }}" class="form-control" />
                            @if ($errors->has('points'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;" name="points_strong">{{ $errors->first('points') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="row m-0 answers-area">
                            <div class="ans-area col-flex-quiz">
                                <div class="row w-100 m-0">
                                    <input type="text" id="ansvalue" class="form-control" placeholder="{{ __('website.typeans') }}"/>
                                </div><!--row-->
                                <div class="row m-0 chec-ans">
                                    <div class="col p-0">
                                        <input type="checkbox" id="cor-1" />
                                        <label for="cor-1">{{ __('website.correctans') }}</label>
                                    </div>
                                    <div class="col ml-auto p-0">
                                        <button class="add-answer" id="addanswer">{{ __('website.AddAnswer') }}</button>
                                    </div>
                                </div><!--row-->
                            </div><!--ans-area-->
                            <input type="hidden" name="answers[]">
                            <input type="hidden" name="correct[]">
                            <div id="aswers-area">
                                <!--ans-area-->
                            </div>
                            <div class="save-quiz">
                                <button name="save_button">{{ __('website.save') }}</button>
                            </div>

                        </div>
                            
                    </div><!--container-exam-->	
                        
                </div>
            </form>
        </div>
    </div>
</div>

@endsection