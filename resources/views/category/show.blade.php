@extends('layouts.app')

@section('content')

<section class="title-page">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3>{{$category->name}}</h3>
				<div class="total-exams">{{$category->exams->count()}} {{ __('website.Resultefor') }} {{$category->name}}</div>
			</div><!--col-->
			{{-- <div class="col-md-6">
				<div class="sort">
					<form action="#">
						<label>Sort By</label>
						<select class="form-control">
							<option> Recommended </option>
							<option> Highest Rated </option>
							<option> Newest </option>
							<option> Lowest Price </option>
							<option> Highest Price </option>
						</select>
					</form>
				</div>
			</div><!--col--> --}}
			
		</div>
	</div><!--container-->
</section>
<main>
	<div class="container">
		@foreach ($category->exams as $exam)
			<div class="exam-container">
				<div class="row">
					<div class="col-md-4">
						<div class="course-img">
								<a href="#"><img src="{{asset($category->image)}}" alt=""/></a>
						</div>
					</div><!--col-->
					<div class="col-md-8">
						<div class="exam-data">
							<div class="exam-data-info">
								<h3><a href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/conditions/{{$exam->id}}">{{$exam->name}}</a></h3>
								<div class="row">
									<div class="col-6">
										<div class="category-name">
											<a href="/{{ LaravelLocalization::getCurrentLocale() }}/category/{{$category->slug}}">{{$category->name}}</a>
										</div><!--category-name-->
									</div>
									<div class="col-6 rate-exam">
										<div class="rate">
												@for($i=0;$i<round($exam->rateavg); $i++)
													<i class="fa fa-star active"></i>
												@endfor
												@for($i=0;$i<(5 -round( $exam->rateavg)); $i++)
													<i class="fa fa-star"></i>
												@endfor
											<span>{{ $exam->rateavg }} ({{ count($exam->rates) }} {{ __('website.ratings') }})</span>
										</div>
									</div>
								</div><!--row-->
										
								
								<div class="row">
									<div class="col">
										<p class="date"><i class="far fa-clock"></i> {{ __('website.Lastupdated') }} {{$exam->updated_at->format('d/m/Y')}}</p>
									</div><!--col-->
									<div class="col">
										<div class="student-num">
											<i class="far fa-user"></i> <span>{{  $exam->students->count() }} {{ __('website.student') }}</span>
										</div>
									</div><!--col-->
									<div class="col">
									<p class="instructor"><i class="fas fa-user-tie"></i> {{ __('website.by') }}: <a href="/users/{{$exam->user->id}}">{{$exam->user->name}}</a></p>
									</div>
								</div><!--row-->
								
								<div class="exam-tag exam-padding">
									<div class="tags">
										@foreach (explode(",",$exam->tags_skills) as $tag)
											<span class="chip">{{ $tag }} </span>
										@endforeach
									</div>
								</div>
							</div>
							{{-- <div class="exam-data-join">
								
								<div class="price-area">
									<div class="new">{{$exam->price}} $</div>
									<div class="old"><span>$44.99</span> &nbsp; 67% off</div> 
								</div>
								<a href="" data-toggle="modal" data-target="#paymentmodal" class="join-now">{{ __('website.requestcode') }}</a>
								
								
							</div> --}}
						</div>
					</div><!--col-->
				</div><!--row-->
			</div><!--exam-container-->
		@endforeach
			
		
			
	</div><!--container-->
	<!-- Modal -->
<div class="modal fade" id="paymentmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Payment Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" name="exam_id" id="exam-id-input">
				<div class="form-group">
					<label for="firstname">First name</label>
					<input type="text" name="cardname" class="form-control" id="cardname" placeholder="Name On the Card">
				</div>
				<div class="form-group">
					<label for="firstname">First name</label>
					<input type="text" name="cardnumber" class="form-control" id="cardnumber" placeholder="Card Number">
				</div>
				<div class="form-group">
					<label for="firstname">First name</label>
					<input type="text" name="expirdate" class="form-control" id="expirdate" placeholder="Expiration Date (M/Y)">
				</div>
				<div class="form-group">
					<label for="firstname">First name</label>
					<input type="text" name="securitycode" class="form-control" id="securitycode" placeholder="Security Code">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Process Payment</button>
			</div>
		</div>
	</div>
</div>
</main>
@endsection