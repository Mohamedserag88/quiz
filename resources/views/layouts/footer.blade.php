<section class="statistics">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col">
				<p><span>{{ \App\User::all()->count() }}</span> {{ __('website.footeruser') }}</p>
			</div><!--col-->
			<div class="col-md-6 col">
				<p><span>{{\App\Models\Exam::where('draft_public','=',1)->count()}}</span> {{ __('website.footerexam') }}</p>
			</div><!--col-->
		</div><!--row-->
	</div><!--container-->
</section>
<footer>
	<div class="container">
		<div class="row">
			<div class="top-footer">
				<ul>
					<li><a href="#">{{ __('website.Privacy_Policy') }}</a></li>
					<li><a href="#">{{ __('website.Terms_and_Conditions') }}</a></li>
					<li><a href="#">{{ __('website.Help') }}</a></li>
					<li><a href="#">{{ __('website.Quiziu_Licenses') }}</a></li>
					<li><a href="#">{{ __('website.Partners') }}</a></li>
				</ul>
			</div><!--top-footer-->
			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-12 copy-right p-0">

							<p>{{ __('website.Rights_Reserved') }}</p>

						</div>
						<div class="col-md-6 col-12 social p-0 ">

							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>

						</div>
					</div><!--row-->
				</div><!--container-->
			</div><!--bottom-footer-->
		</div><!--row-->
	</div><!--container-->
</footer>