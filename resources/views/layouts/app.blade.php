<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

    <!-- Head
    ================================================== -->
    @include('layouts.head')
    @stack('head')

    <!-- Body
    ================================================== -->
    <body class="boxed" >

        <div id="wrapper">

            <!-- Header
            ================================================== -->
            @include('layouts.header')    

            <!-- Content
            ================================================== -->
            @yield('content')

            <!-- Footer
            ================================================== -->
            @include('layouts.footer')

        </div>

        <!-- Scripts
        ====================================================== -->
        @include('layouts.scripts')
        @stack('scripts')
        @yield('scripts')
    </body>

</html>
