<script type="text/javascript" src="{{ asset('js/scripts.min.js') }}"></script>
<script src="{{ asset('js/jquery.barrating.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
       $('#example-fontawesome-o').barrating({
         theme: 'fontawesome-stars'
       });
    });
 </script>
<script>
    $(document).on('click','#exampublisher',function(){
        $('#exampublishertab').css("display", "block");
        $('#exampublisher').addClass("active");
        $('#studenttab').css("display", "none");
        $('#student').removeClass("active");
        $.get('/setusertype/exampublisher',function(data){

        });
    });
    $(document).on('click','#student',function(){
        $('#studenttab').css("display", "block");
        $('#student').addClass("active");
        $('#exampublishertab').css("display", "none");
        $('#exampublisher').removeClass("active");
        $.get('/setusertype/student',function(data){
            
        });
    });
    $(document).on('change keypress keydown','#examConditions',function(e){
        
            if(e.keyCode == 8){
                //alert("here");
                if(parseInt($('#exam-Conditions').text().split(" ")[0]) == 4700)
                    var text="4700 "+$('#exam-Conditions').text().split(" ")[1] +" "+$('#exam-Conditions').text().split(" ")[2];
                else
                    var text=''+(parseInt($('#exam-Conditions').text().split(" ")[0]) + 1)+" "+$('#exam-Conditions').text().split(" ")[1] +" "+$('#exam-Conditions').text().split(" ")[2];                
                    
            }
            else{
                console.log($('#exam-Conditions').text().split(" ")[0])
                var text=''+(4700 - $('#examConditions').val().length)+" "+$('#exam-Conditions').text().split(" ")[1] +" "+$('#exam-Conditions').text().split(" ")[2];
            }
            
        $('#exam-Conditions').text(text);
    });
    $(document).on('change','#codetype',function(){
        var type=$(this).val();
        if(type=='Student'){
            $('#number_of_students_code').prop("readonly", true);
        }
        else{
            $('#number_of_students_code').prop("readonly", false);
        }
    });
    $(document).on('click','#generatecode',function(e){
        e.preventDefault();
        var type=$('#codetype').val();
        var number=$('#number_of_students_code').val();
        if(type=='Group')
            if(number=='' || !$.isNumeric(number)){
                alert('Please Enter Vaild Number');
            }
            else{
                var code =generatecode();
                var element='<div class="row " style="margin-bottom: 15px;">'+
                            '<div class="col-lg-3">'+
                                '<input type="text" readonly name="type[]" class="form-control"   value="'+type+'">'+
                            '</div>'+
                            '<div class="col-lg-2">'+
                                '<input type="number" readonly name="student_number[]" class="form-control"   value="'+number+'">'+
                            '</div>'+
                            '<div class="col-lg-5">'+
                                '<input type="text" readonly name="code[]" class="form-control"   value="'+code+'">'+
                            '</div>'+
                            '<div class="col-lg-2">'+
                                '<button type="button" class="btn btn-secondary btn-sm removebtn" style="width: 100%;height: 100%;"><i class="fas fa-minus-circle"></i></button>'+
                            '</div>'+
                        '</div>';
                $('#codesdiv').append(element);
                $('#number_of_students_code').val('');
            }
        else if(type=='Student'){
            var code =generatecode();
            number=1;

            var element='<div class="row " style="margin-bottom: 15px;">'+
                            '<div class="col-lg-3">'+
                                '<input type="text" readonly name="type[]" class="form-control"   value="'+type+'">'+
                            '</div>'+
                            '<div class="col-lg-2">'+
                                '<input type="number" readonly name="student_number[]" class="form-control"   value="'+number+'">'+
                            '</div>'+
                            '<div class="col-lg-5">'+
                                '<input type="text" readonly name="code[]" class="form-control"   value="'+code+'">'+
                            '</div>'+
                            '<div class="col-lg-2">'+
                                '<button type="button" class="btn btn-secondary btn-sm removebtn" style="width: 100%;height: 100%;"><i class="fas fa-minus-circle"></i></button>'+
                            '</div>'+
                        '</div>';
            $('#codesdiv').append(element);
            $('#number_of_students_code').val('');
        }
        else{
            alert('Please Choose Code Type');
        }
    });
    $(document).on('click','.removebtn',function(){
        $(this).parent().parent().remove();
    });
    $(document).on('click','#publicpost',function(e){
        e.preventDefault();
        $('#draft_public').val($(this).attr('data-forpost'));
        $(this).css("background-color", "blue");
        $('#draftpost').css("background-color", "red");
    });
    $(document).on('click','#draftpost',function(e){
        e.preventDefault();
        $('#draft_public').val($(this).attr('data-forpost'));
        $(this).css("background-color", "blue");
        $('#publicpost').css("background-color", "red");
    });
    $(document).on('click','#followbtn',function(e){
        e.preventDefault();
        var following=$(this).attr('following-id');
        var follower=$(this).attr('follower-id');
        $.get('/follow/'+following+'/'+follower,function(data){
            if(data=="Following"){
                $('#followbtn').html('<i class="fas fa-rss"></i> '+data);
            }
            else{
                $('#followbtn').html('<i class="fas fa-rss"></i> '+data);
            }
        });
    });
    $(document).on('click','#addanswer',function(e){
        e.preventDefault();
        var ans=$('#ansvalue').val();
        if($('#question-type').val()==1 || $('#question-type').val()==2){
            if($("#cor-1").is(':checked'))
                $('#aswers-area').append(appendaswerchoose(ans,1));
            else
                $('#aswers-area').append(appendaswerchoose(ans,0));
            
        }
        else{
            $('#aswers-area').append(appendanswerfill(ans));
        }
        $("#cor-1").prop('checked', false); 
        $('#ansvalue').val('');
    });
    $(document).on('change','#sitelanguage',function(e){
        
        var local=$(this).val();
        $.get('/setlanguage/'+local,function(data){
            alert(data)
            location.reload();
        });
    });
    $(document).on('click','.removeansbtn',function(e){
        $(this).parent().parent().remove();
    });
    $(document).on('click','#sharecodemodel',function(e){
        e.preventDefault();
        
        var code= $(this).attr('exam-code');
        $.get('/getexamcodes/'+code,function(data){
            $('#sharmodelbody').html(data);
            
        });
        
    });
    $(document).on('click','.sharbtncode',function(e){
        var code= $(this).attr('code-id');
        $('#codeidinput').val(code);
        
    });
    function generatecode(){
        var code =Math.random().toString(36).substring(2, 25) + Math.random().toString(36).substring(2, 25);
        return code;
    }
    function appendaswerchoose(ans,correct){
        var element='<div class="ans-divbody">'+   
                        '<div class="">'+
                            '<button type="button" style="float: right;" class="btn btn-danger btn-smv removeansbtn">{{ __("website.removeAnswer") }}</button>'+
                        '</div>'+
                        '<div class="ans-area">'+
                            '<input type="hidden" name="answers[]" value="'+ans+'">'+
                            '<input type="hidden" name="correct[]" value="'+correct+'">';
                            if(correct){
            element+=       '<input type="checkbox" disabled checked/>';
                            }
                            else{
            element+=       '<input type="checkbox" disabled/>';

                            }
            element+=       '<div class="ans-text">'+
                                ans
                            '</div>'+
                        '</div>';
        return element;
    }
    function appendanswerfill(ans){
        var element='<div class="ans-divbody">'+    
                        '<div class="">'+
                            '<button type="button" style="float: right;" class="btn btn-danger btn-smv removeansbtn">{{ __("website.removeAnswer") }}</button>'+
                        '</div>'+
                        '<div class="ans-area">'+
                            '<input type="hidden" name="answers[]" value="'+ans+'">'+
                            '<input type="hidden" name="correct[]" value="1">'+
                            '<div class="ans-text">'+
                                ans
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
        return element;
    }
</script>