<header>
  <div class="container">
      <div class="row">
          <div class="col-lg-4 col-12 p-0">
              <div class="d-md-flex">
                  <div class="col p-0">
                      <div class="logo">
                          <a href="/{{ LaravelLocalization::getCurrentLocale() }}/" title="Home"><img src="{{ asset('images/logo.png') }}" alt="" /></a>
                      </div>
                  </div>
                  <div class="col p-0">
                      <div class="category">
                          <div class="dropdown">
                              <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('website.Categories') }}
                              </button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach(\App\Models\Category::all() as $category)
                                        <a class="dropdown-item" href="/{{ LaravelLocalization::getCurrentLocale() }}/category/{{$category->slug}}">{{ $category->name }}</a>
                                    @endforeach
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div><!--col-->

          <div class="col-lg-4 col-12 p-0 search">
            <form method="GET" action="/{{ LaravelLocalization::getCurrentLocale() }}/search">
                  <input type="text" name="search" placeholder="{{ __('website.examplaceholder') }}" />
                  <button><i class="fa fa-search"></i></button>
              </form>
          </div><!--col-->

          <div class="col-lg-4 col-12 menu-top">
              <ul class="before-login">
                        @if( LaravelLocalization::getCurrentLocale() == 'en')
                            <li>
                                <a rel="alternate" hreflang="ar" href="{{ LaravelLocalization::getLocalizedURL('ar', null, [], true) }}">
                                    العربيه 
                                </a>
                            </li>  
                        @else
                            <li>
                                <a rel="alternate" hreflang="en" href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">
                                    English 
                                </a>
                            </li>
                        @endif 
                {{-- <li >
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">{{ __('website.Site_Language') }} 
                        <ul class="dropdown-menu">                             
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{($properties['native']=='Arabic')?'العربيه':$properties['native']}} 
                                    </a>
                            @endforeach
                                
                        </ul>
                    </div>
                </li> --}}
                @if(\Auth::check())
                <li>
                    <div class="user-area">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="img"><span>{{ \Auth::user()->name[0] }}</span></div>
                                <div class="name-user">{{ \Auth::user()->name }}</div>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="user-menu">
                                @if(\Auth::user()->type=='exampublisher')
                                    <a class="dropdown-item" href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/create">{{ __('website.createexam') }}</a>
                                @else
                                    <a class="dropdown-item" href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/codepage">{{ __('website.exam') }}</a>                                    
                                @endif
                                <a class="dropdown-item" href="/{{ LaravelLocalization::getCurrentLocale() }}/profile">{{ __('website.profile') }}</a>
                                <a class="dropdown-item" href="/{{ LaravelLocalization::getCurrentLocale() }}/profile/edit">{{ __('website.editprofile') }}</a>
                                <a class="dropdown-item" href="/{{ LaravelLocalization::getCurrentLocale() }}/logout">{{ __('website.logout') }}</a>
                        </div>
                        </div>
                    </div>
                </li>
                @else
                <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/login">{{ __('website.login') }}</a></li>
                <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/register">{{ __('website.signup') }}</a></li>
                @endif
                  
              </ul>
          </div>


      </div><!--row-->
  </div><!--container-->
</header>