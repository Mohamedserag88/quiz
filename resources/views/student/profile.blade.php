@extends('layouts.app')

@section('content')
<section class="page-profile">
	<form action="#">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="puplisher-image-profile">
                        <img src="{{asset(($user->profile)?$user->profile:'images/defultimg.png')}}" class="now-img" alt="" style="height: 100%;"/>	
                        
                    </div><!--puplisher-image-profile-->
                    <div class="puplisher-name">
                        <h4>{{$user->name}}</h4>
                        {{-- <p>UX & UI Designer @ sarmady</p> --}}
                    </div><!--puplisher-name-->
                    <div class="action-area">
                            {{-- <a href="/followings" class="following"><i class="fas fa-rss"></i> following </a> --}}
                    </div>
                    <div class="samary-area">
                        <ul>
                            {{-- <li><i class="fas fa-globe"></i> <a href="#">www.asa4web.com</a></li> --}}
                            <li><i class="fas fa-phone"></i>{{($user->student)?$user->student->phone:''}}</li>
                            <li><i class="far fa-envelope"></i>{{$user->email}}</li>
                        </ul>
                    </div>
                </div><!--col-->
                <div class="col-md-9 p-0">
                    <div class="cover-profile">
                        <img width="100%" src="{{asset(($user->cover)?$user->cover:'/images/defaultcover.gif')}}" alt="" style="height: 100%;"/>
                    </div><!--cover-profile-->
                    <div class="info-puplisher">
                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li> <span><i class="fas fa-rss"></i> {{ count($user->followers) }} {{ __('website.Followers') }}</span></li>
                                    <li><span><i class="far fa-newspaper"></i>  {{$user->exampassed->count()}}  {{ __('website.ExamsPassed') }}</span></li>
                                </ul>
                            </div><!--col-->
                            <div class="col-md-6">
                                <ul class="link-prof"> 
                                    <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/profile/edit">{{ __('website.editprofile') }}</a></li>
                                </ul>
                            </div><!--col-->
                        </div><!--row-->
                        
                        
                    </div><!--info-puplisher-->
                    <div class="row m-0">
                        <div class="about-illust" >
                            <h3>{{ __('website.bio') }}</h3>
                            <article>{{$user->student->pio}}
                            </article>
                        </div><!--about-illust-->
                        <div class="border-bottom"></div>
                        
                        <div class="exams-lecturer trend-exam">
                                
                                <div class="row">
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                            <div class="row m-0">
                                                @foreach($user->exampassed as $exam)
                                                    <div class="col-md-4">
                                                        <div class="trend-course">
                                                            <div class="course-img">
                                                                <a href=""><img src="{{ asset('images/c1.png') }}" alt=""/></a>
                                                            </div>
                                                            <div class="course-content">
                                                                <h3><a href="">{{$exam->name}}</a></h3>
                                                                <p class="date"><i class="far fa-clock"></i> {{ __('website.Lastupdated') }} {{$exam->updated_at}}</p>
                                                                <div class="student-num">
                                                                    <i class="far fa-user"></i> <span>{{$exam->pivot->points}} {{ __('website.Points') }}</span>
                                                                </div>
                                                                    <div class="rate">
                                                                            @for($i=0;$i<round($exam->rateavg); $i++)
                                                                                <i class="fa fa-star active"></i>
                                                                            @endfor
                                                                            @for($i=0;$i<(5 -round( $exam->rateavg)); $i++)
                                                                                <i class="fa fa-star"></i>
                                                                            @endfor
                                                                            <span>{{ $exam->rateavg }} ({{ count($exam->rates) }} {{ __('website.ratings') }})</span>
                                                                    </div>

                                                                <a href="#"></a>
                                                                {{-- <a href="#" class="exam-edit"><i class="far fa-edit"></i> Edit </a> --}}
                                                            </div>
                                                        </div>
                                                    </div><!--col-->
                                                @endforeach
                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <div class="row m-0">
                                                <div class="col-md-4">
                                                    <div class="trend-course">
                                                        <div class="course-img">
                                                            <a href="exam-details.html"><img src="{{ asset('images/c3.png') }}" alt=""/></a>
                                                        </div>
                                                        <div class="course-content">
                                                            <h3><a href="exam-details.html">Earn Your CSS Certification</a></h3>
                                                            
                                                                

                                                            <a href="#"></a>
                                                            <a href="#" class="exam-edit"><i class="far fa-edit"></i> Edit </a>
                                                        </div>
                                                    </div>
                                                </div><!--col-->
                                            </div><!--row-->
                                        </div><!--tab-pane-->

                                    </div>

                                </div><!--row-->
                            
                            
                    
                        </div><!--exams-lecturer-->
                    </div><!--row-->
                </div><!--col-->
            </div><!--row-->
        </div><!--container-->	
	</form>
</section>

@endsection