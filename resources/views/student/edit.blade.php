@extends('layouts.app')

@section('content')
<section class="header-profile">
        <div class="container">
            <div class="row">
                <ul>
                    <li><a name="profile_a" href="/{{ LaravelLocalization::getCurrentLocale() }}/profile" >{{__('website.profile')}}</a></li>
                    <li><a name="profile_edit_a" href="#" class="active"> {{__('website.editprofile')}}</a></li>
                </ul>
            </div>
        </div>
    </section><!--header-profile-->
    
    <section class="page-profile">
        {!! Form::model($user, ['route' => ['students.update', $user->id], 'method' => 'patch','files'=>'true']) !!}
            {{ csrf_field() }}
	 		<div class="container">
				<div class="row">
					<div class="col-8  mr-auto ml-auto border-container">
                        @include('flash::message')
                        <h3 class="title-section">{{__('website.editprofile')}}</h3>
                        <p>{{__('website.editprofilep')}}</p>
                        <div class="form-group">
                            <label name="profile_image_label">{{__('website.profileimage')}}</label>
                            <input type="file" placeholder="" class="form-control" name="profile"/>
                        </div>
                        <div class="form-group">
                                <label name="cover_image_label">{{__('website.coverimage')}}</label>
                                <input type="file" placeholder="" class="form-control" name="cover"/>
                        </div>
                        <div class="form-group">
                            <label name="first_name_label" for="firstname">{{__('website.Firstname')}}</label>
                            <input type="text" name="first_name" class="form-control" id="firstname" placeholder="Enter Your First name" value="{{$user->student->f_name}}">
                            @if ($errors->has('f_name'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->
                        <div class="form-group">
                            <label name="last_name_label" for="lastname">{{__('website.lastname')}}</label>
                            <input type="text" name="last_name" class="form-control" id="lastname" placeholder="Enter Your Last name" value="{{$user->student->l_name}}">
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->
                        {{-- <div class="form-group">
                            <label for="myjob">My Job</label>
                            <input type="text" class="form-control" id="myjob" placeholder="" value="UX & UI Designer @ sarmady">
                        </div><!--form-group--> --}}
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" class="form-control" id="email" placeholder="Enter Your Email" value="{{$user->email}}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->
                        <div class="form-group">
                            <label for="phone" name="phone_label">{{__('website.Phone')}}</label>
                            <input type="text" name="phone" class="form-control" id="phone" placeholder="" value="{{$user->student->phone}}">
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->
                        {{-- <div class="form-group">
                            <label for="my-website">My Website</label>
                            <input type="text" class="form-control" id="my-website" placeholder="" value="www.asa4web.com">
                        </div><!--form-group--> --}}
                        <div class="form-group">
                            <label name="password_label" for="password">{{__('website.Password')}}</label>
                            <input type="text" name="password" class="form-control" id="password" placeholder="Enter Your password">
                        </div><!--form-group-->
                        <div class="form-group">
                            <label name="confirm_passward_label" for="re-password">{{__('website.PasswordConfirm')}}</label>
                            <input type="text" name="password_confirmation" class="form-control" id="re-password" placeholder="Retype your password">
                        </div><!--form-group-->
                        <div class="form-group">
                            <label for="re-password" name="my_bio_label">{{__('website.bio')}}</label>
                            <textarea class="form-control" name="pio">{{$user->student->pio}}</textarea>
                                      
                        </div><!--form-group-->
                        {{-- <div class="form-group">
                            <label for="pr-lang">Your Profile Language</label>
                            <select class="form-control" id="pr-lang" name="profile_language">
                                <option value="English">English</option>
                                <option value="Arabic">Arabic</option>
                            </select>
                        </div><!--form-group--> --}}
                        <div class="form-group text-right">
                            <button class="button save-button" name="save_btn">{{__('website.save')}}</button>
                            <a href="/profile" class="button cancle-button">{{__('website.Cancle')}}</a>
                        </div><!--form-group-->
                    </div>
				</div><!--row-->
			</div><!--container-->
		</form>
	</section>

@endsection