<header class="main-header">
  <!-- Logo -->
  <a href="index2.html" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Admin</b>Panal</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    @include('admin.layouts.menu')
  </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
      <img src="{{ asset(Auth::user()->profile) }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header"></li>

      <li class="treeview {!! (Request::is('admin'))?'active':'' !!}">
        <a href="#">
          <i class="fa fa-list"></i> <span>Admin Dashbord</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu" style="">
          <li class=""><a href="/admin">
            <i class="fa fa-cog"></i> <span>{{ trans('admin.dashboard') }}</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        
    </ul>
  </li>
  <li class="treeview ">
    <a href="#">
      <i class="fa fa-users"></i> <span>{{ trans('admin.admin') }}</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu" style="">
      <li class=""><a href="/admin"><i class="fa fa-users"></i> {{ trans('admin.admin') }}</a></li>
    </ul>
  </li>
  <li class="treeview {!! (Request::is('admin/users'))?'active':'' !!}">
    <a href="#">
      <i class="fa fa-users"></i> <span>Users</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu" style="">
      <li class=""><a href="/admin/users"><i class="fa fa-users"></i> {{ trans('admin.users') }}</a></li>
      <li class=""><a href="/admin/users?type=student"><i class="fa fa-users"></i> {{ trans('admin.student') }}</a></li>
      <li class=""><a href="/admin/users?type=exampublisher"><i class="fa fa-users"></i> {{ trans('admin.exampblisher') }}</a></li>
      
    </ul>
  </li>
  <li class="">
    <a href="/admin/exams">
      <i class="fa fa-files-o"></i> <span>Exams</span>
      
    </a>
  </li>
  <li class="">
    <a href="/admin/codes">
      <i class="fa fa-files-o"></i> <span>Codes</span>
      
    </a>
  </li>
  <li class="">
    <a href="/admin/questions">
      <i class="fa fa-files-o"></i> <span>Questions</span>
      
    </a>
  </li>
  <li class=" ">
    <a href="/admin/category">
      <i class="ion ion-pie-graph"></i> <span>Categoryies</span>
      
    </a>
  </li>
  
  
</ul>
</section>
<!-- /.sidebar -->
</aside>