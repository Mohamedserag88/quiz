@extends('admin.index')
@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Categories</h3>
    <a href="/admin/category/create"> Add New</a>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Slug</th>
          <th>Descrpition</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($categories as $category)
        <tr>
            <td>{{$category->name}}</td>
            <td>{{$category->slug}}</td>
            <td>{{ $category->descrpition}}</td>
            <td>
                {!! Form::open(['route' => ['admin.category.destroy', $category->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.category.show', [$category->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.category.edit', [$category->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
          </tr>
        @endforeach
       
      </tbody>
      <tfoot>
      
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>

@endsection
@push('js')
  <script>
    $(function () {
      $('#example1').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
@endpush