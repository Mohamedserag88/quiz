@extends('admin.index')
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">Add New Category </h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>'admin/category']) !!}
      <div class="form-group">
        {!! Form::label('name_en','Name In English') !!}
        {!! Form::text('name_en',old('name_en'),['class'=>'form-control','id'=>'catname']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('name_ar','الاسم بالعربى') !!}
        {!! Form::text('name_ar',old('name_ar'),['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('slug','Slug:') !!}
        {!! Form::text('slug',null,['class'=>'form-control','id'=>'slug','readonly']) !!}
      </div>
     <div class="form-group">
        {!! Form::label('descrpition','Descrpition:') !!}
        {!! Form::textarea('descrpition',null,['class'=>'form-control']) !!}
      </div>
     {!! Form::submit(trans('website.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection
@push('js')
<script>
  $(document).on('keypress change','#catname',function(){
        var slug=string_to_slug($('#catname').val());
        $('#slug').val(slug);
    });
    function string_to_slug (str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();
    
        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '') // collapse whitespace and replace by -
            .replace(/-+/g, ''); // collapse dashes

        return str;
    }
</script>
@endpush