@extends('admin.index')
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">Edit Category</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::model($category,['url'=>'admin/category/'.$category->id,'method'=>'put' ]) !!}
    <div class="form-group">
      {!! Form::label('name_en','Name In English') !!}
      {!! Form::text('name_en',old('name_en'),['class'=>'form-control','id'=>'catname']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('name_ar','الاسم بالعربى') !!}
      {!! Form::text('name_ar',old('name_ar'),['class'=>'form-control']) !!}
    </div>
   <div class="form-group">
      {!! Form::label('descrpition','Descrpition:') !!}
      {!! Form::textarea('descrpition',null,['class'=>'form-control']) !!}
   </div>
   {!! Form::submit(trans('website.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection