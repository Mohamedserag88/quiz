@extends('admin.index')
@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Questions</h3>
    <a href="/admin/questions/create"> Add New</a>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Question</th>
          <th>Hint</th>
          <th>Point</th>
          <th>Type</th>
          <th>Exam</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($questions as $question)
        <tr>
            <td>{{$question->question}}</td>
            <td>{{$question->hint}}</td>
            <td>{{ $question->points}}</td>
            <td>{{ $question->type->name}}</td>
            <td>{{ $question->exam->name}}</td>
            <td>
                {!! Form::open(['route' => ['admin.questions.destroy', $question->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.questions.edit', [$question->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
          </tr>
        @endforeach
       
      </tbody>
      <tfoot>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Type</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>

@endsection
@push('js')
  <script>
    $(function () {
      $('#example1').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
@endpush