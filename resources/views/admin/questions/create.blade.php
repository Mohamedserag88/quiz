@extends('admin.index')
@push('css')
<link rel="stylesheet" href="{{ asset('css/jquery.tag-editor.css') }}">
@endpush
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ __('website.createexam') }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>'admin/exams']) !!}
        <div class="form-group">
        {!! Form::label('exam_id','Type:') !!}
        {!! Form::select('type_id',$types->pluck('name','id'),null,['class'=>'form-control','id'=>'question-type']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('question','question:') !!}
        {!! Form::text('question',old('name'),['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('hint',__('website.hint')) !!}
        {!! Form::textarea('hint',null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('exam_id',__('website.exam')) !!}
        {!! Form::select('exam_id',$exams->pluck('name','id'),null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('points', __('website.Points') ) !!}
        {!! Form::number('points',null,['class'=>'form-control']) !!}
      </div>
      <div class="row m-0 answers-area">
          <div class="ans-area col-flex-quiz">
              <div class="form-group">

                  <input type="text" id="ansvalue" class="form-control" placeholder="{{ __('website.typeans') }}"/>
              </div><!--row-->
              <div class="row m-0 chec-ans">
                  <div class="col p-0">
                      <input type="checkbox" id="cor-1" />
                      <label for="cor-1">{{ __('website.correctans') }}</label>
                  </div>
                  <div class="col ml-auto p-0">
                      <button class="add-answer" id="addanswer">{{ __('website.AddAnswer') }}</button>
                  </div>
              </div><!--row-->
          </div><!--ans-area-->
          <input type="hidden" name="answers[]">
          <input type="hidden" name="correct[]">
          <div id="aswers-area">
              <!--ans-area-->
          </div>

      </div>
      {!! Form::submit(trans('website.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection

@push('js')
<script>
$(document).on('click','#addanswer',function(e){
  e.preventDefault();
  var ans=$('#ansvalue').val();
  if($('#question-type').val()==1 || $('#question-type').val()==2){
      if($("#cor-1").is(':checked'))
          $('#aswers-area').append(appendaswerchoose(ans,1));
      else
          $('#aswers-area').append(appendaswerchoose(ans,0));
      
  }
  else{
      $('#aswers-area').append(appendanswerfill(ans));
  }
  $("#cor-1").prop('checked', false); 
  $('#ansvalue').val('');
});
function appendaswerchoose(ans,correct){
        var element='<div class="ans-divbody">'+   
                        '<div class="">'+
                            '<button type="button" style="float: right;" class="btn btn-danger btn-smv removeansbtn">{{ __("website.removeAnswer") }}</button>'+
                        '</div>'+
                        '<div class="ans-area">'+
                            '<input type="hidden" name="answers[]" value="'+ans+'">'+
                            '<input type="hidden" name="correct[]" value="'+correct+'">';
                            if(correct){
            element+=       '<input type="checkbox" checked/>';
                            }
                            else{
            element+=       '<input type="checkbox"/>';

                            }
            element+=       '<div class="ans-text">'+
                                ans
                            '</div>'+
                        '</div>';
        return element;
    }
    function appendanswerfill(ans){
        var element='<div class="ans-divbody">'+    
                        '<div class="">'+
                            '<button type="button" style="float: right;" class="btn btn-danger btn-smv removeansbtn">{{ __("website.removeAnswer") }}</button>'+
                        '</div>'+
                        '<div class="ans-area">'+
                            '<input type="hidden" name="answers[]" value="'+ans+'">'+
                            '<input type="hidden" name="correct[]" value="1">'+
                            '<div class="ans-text">'+
                                ans
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
        return element;
    }
    $(document).on('click','.removeansbtn',function(e){
        $(this).parent().parent().remove();
    });
</script>
@endpush