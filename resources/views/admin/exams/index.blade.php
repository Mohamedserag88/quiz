@extends('admin.index')
@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Exams</h3>
    <a href="/admin/exams/create"> Add New</a>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Title</th>
          <th>Hint</th>
          <th>Number Of Question</th>
          <th>Publisher</th>
          <th>Number of passed Student</th>
          <th>Time</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($exams as $exam)
        <tr>
            <td>{{$exam->name}}</td>
            <td>{{$exam->hint}}</td>
            <td>{{ count($exam->questions)}}</td>
            <td>{{ $exam->user->name}}</td>
            <td>{{ count($exam->students)}}</td>
            <td>{{$exam->timer}}</td>
            <td>
                {!! Form::open(['route' => ['admin.exams.destroy', $exam->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.exams.edit', [$exam->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
          </tr>
        @endforeach
       
      </tbody>
      <tfoot>
      
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>

@endsection
@push('js')
  <script>
    $(function () {
      $('#example1').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
@endpush