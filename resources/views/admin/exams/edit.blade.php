@extends('admin.index')
@push('css')
<link rel="stylesheet" href="{{ asset('css/jquery.tag-editor.css') }}">
@endpush
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">Edit Exam</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::model($exam,['url'=>'admin/exams/'.$exam->id,'method'=>'put' ]) !!}
      <div class="form-group">
        {!! Form::label('name',trans('admin.name')) !!}
        {!! Form::text('name',old('name'),['class'=>'form-control','id'=>'catname']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('conditions',__('website.examcondition')) !!}
        {!! Form::textarea('conditions',null,['class'=>'form-control']) !!}
      </div>
      <input type="hidden" name="price" value="0">
      <div class="form-group">
        {!! Form::label('language', __('website.examcatselect') ) !!}
        {!! Form::select('language',['English'=>'English','Arabic'=>'العربيه','Frensh'=>'Frensh'],null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('category_id',trans('admin.name')) !!}
        {!! Form::select('category_id',$categories->pluck('name','id'),null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('tags', __('website.tags') ) !!}
        {!! Form::text('tags_skills',null,['class'=>'form-control','id'=>'Tags']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('timer', __('website.examtime') ) !!}
        {!! Form::text('timer',null,['class'=>'form-control']) !!}
      </div>
      {!! Form::submit(trans('website.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection
@push('js')
  <script src="{{ asset('js/jquery.tag-editor.min.js') }}"></script>
  <script src="{{ asset('js/tag.js') }}"></script>
@endpush