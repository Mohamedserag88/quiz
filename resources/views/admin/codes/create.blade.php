@extends('admin.index')
@push('css')
<link rel="stylesheet" href="{{ asset('css/jquery.tag-editor.css') }}">
@endpush
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ __('website.createexam') }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>'admin/codes']) !!}
      <div class="form-group">
        {!! Form::label('code',__('website.code')) !!}
        {!! Form::text('code',null,['class'=>'form-control','readonly','id'=>'codeinput']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('exam_id', __('website.exam') ) !!}
        {!! Form::select('exam_id',$exams->pluck('name','id'),null,['class'=>'form-control']) !!}
      </div>
      <div class="form-group">
        {!! Form::label('type',trans('admin.codetype')) !!}
        {!! Form::select('type',['Group'=>'Group','Student'=>'Student'], null, ['class' => 'form-control','id'=>'codetype','required'=>'required','placeholder'=> __('website.codetype')]) !!}
      </div>
      
      <div class="form-group">
        {!! Form::label('student_number', __('website.enterstudent') ) !!}
        {!! Form::text('student_number',null,['class'=>'form-control']) !!}
      </div>
      <button class="'btn btn-primary" type="button" id="generatecode">{{ __('website.Generate') }}</button>

      {!! Form::submit(trans('website.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection

@push('js')
<script>
  $(document).on('click','#generatecode',function(e){
      var code =Math.random().toString(36).substring(2, 25) + Math.random().toString(36).substring(2, 25);
        
      $('#codeinput').val(code);
    });

</script>
@endpush