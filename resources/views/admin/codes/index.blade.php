@extends('admin.index')
@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Codes</h3>
    <a href="/admin/codes/create"> Add New</a>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Code</th>
          <th>Type</th>
          <th>Number Of Student</th>
          <th>Exam</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($codes as $code)
        <tr>
            <td>{{$code->code}}</td>
            <td>{{$code->type}}</td>
            <td>{{ $code->student_number}}</td>
            <td>{{ $code->exam->name}}</td>
            <td>
                {!! Form::open(['route' => ['admin.codes.destroy', $code->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.codes.edit', [$code->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
          </tr>
        @endforeach
       
      </tbody>
      <tfoot>
      
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>

@endsection
@push('js')
  <script>
    $(function () {
      $('#example1').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>
@endpush