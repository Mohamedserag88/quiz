@extends('admin.index')
@section('content')



      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{\App\User::count()}}</h3>

              <p>New Users</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-people-outline"></i>              
            </div>
            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/admin/users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{\App\User::where('type','admin')->get()->count()}}</h3>

              <p>Admins</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-people-outline"></i>              
            </div>
            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/admin/dashbord" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{\App\Models\Category::count()}}</h3>

              <p>Website Categories</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/admin/category" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{\App\User::where('type','student')->get()->count()}}</h3>

              <p>Students</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-people-outline"></i>
            </div>
            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/admin/users?type=student" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{\App\User::where('type','exampublisher')->get()->count()}}</h3>

              <p>Exam Publisher</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-people-outline"></i>
            </div>
            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/admin/users?type=exampublisher" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{\App\Models\Exam::count()}}</h3>

              <p>Exams</p>
            </div>
            <div class="icon">
              <i class="fa fa-files-o"></i>
            </div>
            <a href="/{{ LaravelLocalization::getCurrentLocale() }}/admin/exams" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       
      </div>
      <!-- /.row -->
     

@endsection