@extends('layouts.app')

@section('content')
<section class="title-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>{{$query}}</h3>
                <div class="total-exams">{{  count($categories) + count($exams) + count($users) }} {{ __('website.Resultefor') }} {{$query}}</div>
            </div><!--col-->
        </div>
    </div><!--container-->
</section>
    <main>
        <div class="container">
            <div class="row" >
                
                <div class="col-md-4" >
                    <h3 style="padding: 10px">{{ __('website.exams') }}</h3>
                    <div class="row" style="margin-left: 0px;">
                        @foreach ($exams as $exam)
                            <div class="col-md-12" style="margin-bottom: 30px;padding:10px;border:1px solid #e6e6e6">
                                <div class="trend-course">
                                    <div class="course-img" style="border:0">
                                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/conditions/{{ $exam->id }}"><img src="{{ asset($exam->category->image) }}" alt=""></a>
                                    </div>
                                    <div class="course-content" style="padding-left:15px">
                                        <h3><a href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/conditions/{{ $exam->id }}">{{ $exam->name }}</a></h3>
                                        <p class="date"><i class="far fa-clock"></i> {{ __('website.Lastupdated') }} {{ $exam->updated_at->format('d/m/Y') }}</p>
                                        <p class="instructor"><i class="fas fa-user-tie"></i> {{ __('website.by') }}: <a href="/users/{{ $exam->user->id }}">{{ $exam->user->name }}</a></p>
                                        <div class="student-num">
                                            <i class="far fa-user"></i> <span>{{ $exam->number_of_students }} {{ __('website.student') }}</span>
                                        </div>
                                        <div class="rate">
                                            @for($i=0;$i<round($exam->rateavg); $i++)
                                                <i class="fa fa-star active"></i>
                                            @endfor
                                            @for($i=0;$i<(5 -round( $exam->rateavg)); $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                            <span>{{ $exam->rateavg }} ({{ count($exam->rates) }} {{ __('website.ratings') }})</span>
                                        </div>
                                    </div>
                                </div><!--trend-course-->
                            </div><!--col-->
                        @endforeach<!--exam-container-->
                    </div>
                </div>
                <div class="col-md-4" >
                    <h3 style="padding: 10px">{{ __('website.ExamPublisher') }}</h3>
                    <div class="row" style="margin-left: 0px;">
                        @foreach ($users as $user)
                            <div class="col-md-12" style="margin-bottom: 30px;padding:10px;border:1px solid #e6e6e6">
                                <a href="/{{ LaravelLocalization::getCurrentLocale() }}/users/{{ $user->id }}">
                                    <div class="publisher-item">
                                        <img src="{{ asset($user->profile) }}" width="100%" alt=""/>
                                        <p>{{ $user->name }}</p>
                                        <p class="colored-p">{{ $user->exampublisher->myjob }}</p>
                                        
                                    </div>
                                </a>
                            </div><!--exam-container-->
                        @endforeach
                    </div>
                </div>
                
                <div class="col-md-4" >
                    <h3 style="padding: 10px">{{ __('website.Categories') }}</h3>
                    <div class="row" style="margin-left: 0px;">
                        @foreach ($categories as $category)
                            <div class="exam-container" style="margin-bottom: 30px;padding:10px;border:1px solid #e6e6e6">
                                <div class="trend-course">
                                    <div class="course-img" style="border:0">
                                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/category/{{$category->slug}}"><img width="100%" src="{{asset($category->image)}}" alt=""/></a>
                                    </div>
                                    <div class="course-content" style="padding-left:15px">
                                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}/category/{{ $category->slug}}">{{  $category->name}}</a>     
                                    </div>
                                </div><!--row-->
                            </div><!--exam-container-->
                        @endforeach
                    </div>
                </div>
            </div>
            
                
        </div><!--container-->
    </main>
@endsection