@extends('layouts.app')

@section('content')
<main style="display: flex">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6 col-sm-10  m-auto pre-exam">
                @include('flash::message')
                <h3>{{ __('website.examcode') }}</h3>
                {!! Form::open(['route' => 'exams.examprepage','method'=>'get']) !!}
                    <div class="form-group">
                        <input type="text" name="code" placeholder="Your Code" class="form-control" />
                    </div>
                    <div class="form-group">
                        <button name="check_btn" class="button">{{ __('website.Check') }}</button>
                    </div>                        
                </form>
            </div>
        </div>

    </div>
</main>
@endsection