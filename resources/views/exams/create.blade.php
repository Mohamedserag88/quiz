@extends('layouts.app')
@push('head')
<link rel="stylesheet" href="{{ asset('css/jquery.tag-editor.css') }}">
@endpush
@section('content')
<main>
    <div class="container">
        {!! Form::open(['route' => 'exams.store']) !!}
            <div class="row">
                <div class="col-md-8 col-12 create-exam">
                    <h3 class="title-section" name="create_exam_h3">{{ __('website.createexam') }}</h3>
                    
                        <div class="form-group">
                            <label for="examName" name="exam_name_label">{{ __('website.examname') }}</label>
                            <input type="text" name="name" class="form-control" id="examName" placeholder="{{ __('website.enterexamname') }}">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->

                        <div class="form-group">
                            <label for="examConditions" name="exam_Conditions_label">{{ __('website.examcondition') }}</label>
                            <textarea class="form-control" name="conditions" id="examConditions" aria-describedby="exam-Conditions" placeholder="{{ __('website.enterexamcondition') }}"></textarea>
                            @if ($errors->has('conditions'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('conditions') }}</strong>
                                </span>
                            @endif
                            <small id="exam-Conditions" class="form-text text-muted">4700 {{ __('website.remainchar') }}</small>
                        </div><!--form-group-->
                        <div class="form-group">
                            <input type="hidden" name="price" value="0">
                                {{-- <label for="examName">{{ __('website.examprice') }}</label>
                                <input type="number" name="price" class="form-control"  placeholder="{{ __('website.enterexamprice') }}">
                                @if ($errors->has('price'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif--}}
                            </div><!--form-group-->
                        <div class="border-bottom m-t-b-50"></div>

                        

                        <!--quiz-container-->
                        
                        
                    
                </div><!--col-->
                <div class="col-md-4 col-12">
                    <div class="exam-setting ">
                        <div class="lang">
                            <h3 class="title-section">{{ __('website.examsetting') }}</h3>
                            <div class="form-group  exam-lang">
                                <label name="exam_language_label">{{ __('website.examlangselect') }}</label>
                                <select class="form-control" name="language">
                                    <option selected disabled>{{ __('website.examlang') }}</option>
                                    <option value="English">English </option>
                                    <option value="Arabic">العربيه</option>
                                    <option value="Frensh">Frensh</option>
                                </select>
                                @if ($errors->has('language'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ $errors->first('language') }}</strong>
                                    </span>
                                @endif
                            </div><!--form-group-->
                        </div>
                        <div class="exam-point exam-padding">
                            <div class="form-group  exam-lang">
                                <label name="category_label">{{ __('website.examcatselect') }}</label>
                                <select class="form-control" name="category_id">
                                    <option selected disabled>{{ __('website.examcat') }}</option>
                                    @foreach ($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}} </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('category_id'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="exam-tag exam-padding">
                            <label name="tags_label">{{ __('website.tags') }}</label>
                            <input type="text" id="Tags" class="form-control"  name="tags_skills">
                            @if ($errors->has('tags_skills'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('tags_skills') }}</strong>
                                </span>
                            @endif
                            <div class="tags">
                                {{-- <span>UI/UX design <i class="far fa-times-circle"></i></span>
                                <span>Html5 <i class="far fa-times-circle"></i></span> --}}
                            </div>
                        </div>
                        <div class="exam-share exam-padding">
                            <label name="exam_time_label">{{ __('website.examtime') }}</label>
                            <input type="number" name="timer" step="0.1" class="form-control" id="examtimer" placeholder="{{ __('website.examtime') }}">
                            @if ($errors->has('timer'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong style="color:red;">{{ $errors->first('timer') }}</strong>
                                </span>
                            @endif
                            
                        </div>
                        {{-- <div class="exam-code exam-padding">
                            <label>{{ __('website.gencode') }}</label>
                            <div class="exam-co">
                                <input type="hidden" name="number_of_students" id="number_of_students">
                                <input type="text" name="code" class="form-control" placeholder="{{ __('website.enterstudent') }}" id="number_of_students_code">
                                @if ($errors->has('number_of_students'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ __('website.examcodeerror') }} {{ $errors->first('number_of_students') }} </strong>
                                    </span>
                                @endif
                                <button type="button" id="generatecode">{{ __('website.Generate') }}</button>
                            </div>
                        </div> --}}
                        {{-- <div class="exam-padding">
                            <div class="exam-buttons">
                                <input type="hidden" name="draft_public" id="draft_public" value="yes">
                                <div class="add-quiz-button">
                                    <button id="publicpost" style="background: red;" data-forpost="yes">{{ __('website.MakeDraft') }}</button>
                                </div>
                                <div class="save-quiz">
                                    <button id="draftpost" style="background: red;" data-forpost="no">{{ __('website.PublicNow') }}</button>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div><!--col-->  
                <div class="add-quiz-button">
                    <button name="save_btn">{{ __('website.addexam') }}</button>
                </div>
            </div><!--row-->
        </form>
    </div><!--container-->
</main>

@endsection
@push('scripts')
<script src="{{ asset('js/jquery.tag-editor.min.js') }}"></script>
<script src="{{ asset('js/tag.js') }}"></script>
@endpush
