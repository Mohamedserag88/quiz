@extends('layouts.app')
@push('head')
<link rel="stylesheet" href="{{ asset('css/jquery.tag-editor.css') }}">
@endpush
@section('content')
<main>
    <div class="container">
        {!! Form::model($exam,['route' => ['exams.update', $exam->id],'files'=>'true','method' => 'patch']) !!}
            <div class="row">
                <div class="col-md-8 col-12 create-exam">
                    <h3 class="title-section">{{ __('website.createexam') }}</h3>
                    
                        <div class="form-group">
                            <label for="examName" name="exam_name_label">{{ __('website.examname') }}</label>
                            {!! Form::text('name', null, ['class' => 'form-control','id'=>'examName','required'=>'required','placeholder'=> __('website.enterexamname')]) !!}
                            
                        </div><!--form-group-->

                        <div class="form-group">
                            <label for="examConditions" name="exam_conditions_label">{{ __('website.examcondition') }}</label>
                            {!! Form::textarea('conditions', null, ['class' => 'form-control','id'=>'examConditions','required'=>'required','placeholder'=> __('website.enterexamcondition')]) !!}
                           
                            <small id="exam-Conditions" class="form-text text-muted">4700 {{ __('website.remainchar') }}</small>
                        </div><!--form-group-->
                        <div class="form-group">
                            <input type="hidden" name="price" value="0">
                                {{-- <label for="examName">{{ __('website.examprice') }}</label>
                                <input type="number" name="price" class="form-control"  placeholder="{{ __('website.enterexamprice') }}">
                                @if ($errors->has('price'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif--}}
                            </div><!--form-group-->
                        <div class="border-bottom m-t-b-50"></div>

                        

                        <!--quiz-container-->
                        
                        
                    
                </div><!--col-->
                <div class="col-md-4 col-12">
                    <div class="exam-setting ">
                        <div class="lang">
                            <h3 class="title-section">{{ __('website.examsetting') }}</h3>
                            <div class="form-group  exam-lang">
                                <label name="exam_language_label">{{ __('website.examlangselect') }}</label>
                            {!! Form::select('language',['English'=>'English','Arabic'=>'العربيه','Frensh'=>'Frensh'], null, ['class' => 'form-control','id'=>'examConditions','required'=>'required','placeholder'=> __('website.examlang')]) !!}                                
                                
                            </div><!--form-group-->
                        </div>
                        <div class="exam-point exam-padding">
                            <div class="form-group  exam-lang">
                                <label name="category_label">{{ __('website.examcatselect') }}</label>
                            {!! Form::select('category_id',$categories->pluck('name','id'), null, ['class' => 'form-control','required'=>'required','placeholder'=> __('website.examcat')]) !!}
                                
                            </div>
                        </div>
                        <div class="exam-tag exam-padding">
                            <label name="label">{{ __('website.tags') }}</label>
                            {!! Form::text('tags_skills', null, ['class' => 'form-control','id'=>'Tags','required'=>'required']) !!}
                            
                            <div class="tags">
                                {{-- <span>UI/UX design <i class="far fa-times-circle"></i></span>
                                <span>Html5 <i class="far fa-times-circle"></i></span> --}}
                            </div>
                        </div>
                        <div class="exam-share exam-padding">
                                <label name="exam_time_label">{{ __('website.examtime') }}</label>
                                {!! Form::number('timer', null, ['class' => 'form-control','id'=>'timer','required'=>'required','step'=>'0.1']) !!} 
                        </div>
                        
                    </div>
                </div><!--col-->  
                <div class="add-quiz-button">
                        <button name="exam_save">{{ __('website.save') }}</button>
                </div>
            </div><!--row-->
        </form>
    </div><!--container-->
</main>

@endsection
@push('scripts')
    <script src="{{ asset('js/jquery.tag-editor.min.js') }}"></script>
    <script src="{{ asset('js/tag.js') }}"></script>
@endpush
