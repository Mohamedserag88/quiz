@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-12 create-exam">
            @include('flash::message')
            <h3 class="title-section">{{ $exam->name }}</h3>
            <div id="timerdiv"></div>
            @php
            $i=1;
            @endphp
            {!! Form::open(['route' => 'exams.correct','id'=>'examform']) !!}
            <input type="hidden" name="exam_id" value="{{ $exam->id }}">
                @foreach ($exam->questions as $question)
                    <div class="quiz-container" id="questionbody">
                        <div class="title-quiz">
                            <span class="num">{{$i}}-  </span>
                            <span name="question_type_span" class="type-quiz">{{$question->type->name}}</span>
                            <span name="question_question_span" class="text-quiz">{{$question->question}}</span>        
                        </div>
                        <div class="container-exam">
                            <div class="row m-0 hint-image">
                                <div class="col-md-4 col image-quiz">
                                    <a name="question_a" href="{{$question->photo}}" data-fancybox="gallery"><img src="{{asset($question->photo)}}" alt="" /></a>
                                </div><!--col-->
                                @if($question->hint)
                                    <div class="col-md-8 col hint-quiz">
                                        <h4>{{ __('website.hint') }}</h4>
                                        <article>{{$question->hint}}</article>
            
                                    </div><!--col-->
                                @endif
                            </div>
                            <div class="row m-0 point-num">
                                <label>{{ __('website.Points') }}:</label> <span>{{$question->points}}</span>
                            </div>
                            <div class="row m-0 answers-area">
                                @if($question->type->id == 1 )
                                    @foreach($question->answers as $ans)
                                        <div class="ans-area">
                                            <input type="radio" name="ans_{{ $question->id }}" value="{{ $ans->id }}"/>
                                            <div class="ans-text">
                                                {{$ans->answers}}
                                            </div>
                                        </div><!--ans-area-->
                                    @endforeach
                                @elseif($question->type->id == 2)
                                    @foreach($question->answers as $ans)
                                        <div class="ans-area">
                                            <input type="checkbox" name="ans_{{ $question->id }}[]" value="{{ $ans->id }}"/>
                                            <div class="ans-text">
                                                    {{  $ans->answers}}
                                            </div>
                                        </div><!--ans-area-->
                                    @endforeach
                                @elseif($question->type->id == 3) 
                                    <div class="ans-area">
                                        <textarea class="form-control" name="ans_{{ $question->id }}" ></textarea>
                                    </div>
                                @else
                                    <div class="ans-area col-sm-3">
                                        <input type="text" class="form-control" name="ans_{{ $question->id }}" ></input>
                                    </div>
                                @endif   
                            </div>							
                        </div><!--container-exam-->
                    </div><!--quiz-container-->
                    @php
                    $i++;
                    @endphp
                @endforeach
                <div class="add-quiz-button">
                        <button >{{ __('website.Correct') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    setInterval(function(){
        var endtime={{ strtotime(Session::get('stop_timer'))  }};
        var d = new Date();
        var starttime = Math.ceil(d.getTime()/1000);
        var timer=endtime-starttime;
        if(timer==0){
            $('#examform').submit();
        }
        var hours = Math.floor(timer / 3600);
        timer = timer - hours * 3600;
        var minutes = Math.floor(timer / 60);
        var seconds = timer - minutes * 60;
        
        $('#timerdiv').html(hours+':'+minutes+':'+seconds);
    },1000);
</script>
@endpush