@extends('layouts.app')

@section('content')
<main>
    <div class="container">
        <div class="text-section">
            <h3 class="title-section">{{ __('website.examcondition') }}</h3>
            <article>
                {{ $exam->conditions }}
            </article>
            <div class="row m-0">
                <a name="start_a" href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/code/{{ $exam->id }}" class="button start-now">{{ __('website.startnow') }}</a>
            </div>
            
        </div>
    </div>
</main>
@endsection