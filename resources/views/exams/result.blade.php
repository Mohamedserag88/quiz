@extends('layouts.app')

@section('content')
<main style="display: flex">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6 col-sm-10  m-auto pre-exam">
                    <div class="form-group">
                        <button class="button" style="background: #49da49;">{{ $mesaage }}</button>
                    </div>
                @include('flash::message')
                <h3>{{ __('website.examresult') }}</h3>
                <h3>{{ $points }}</h3>
                    
                    <div class="form-group">
                        <a href="/{{ LaravelLocalization::getCurrentLocale() }}" class="button">{{ __('website.home') }}</a>
						<a href="" data-toggle="modal" data-target="#reviewsmodal" class="button">{{ __('website.review') }}</a>
                    </div>                        
                
            </div>
        </div>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="reviewsmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('website.reviewtitle') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'rate']) !!}
                    <div class="modal-body">
                        <div class="add-review">
                            <input type="hidden" name="exam" value="{{$exam->id}}">
                            <h3 class="title-section">{{ __('website.review') }} </h3>
                            <div class="rate">
                                <select id="example-fontawesome-o" name="rate" data-current-rating="5.6" autocomplete="off">
                                    <option value=""></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            
                            <div class="form-group mt-4">
                                <textarea class="form-control" name="comment" placeholder="{{ __('website.addcomment') }}"></textarea>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('website.close') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('website.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

@endsection