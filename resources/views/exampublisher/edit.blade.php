@extends('layouts.app')

@section('content')
<section class="header-profile">
        <div class="container">
            <div class="row">
                <ul>
                     <li><a name="profile_a" href="/{{ LaravelLocalization::getCurrentLocale() }}/profile" >{{__('website.profile')}}</a></li>
                    <li><a name="profile_edit_a" href="#" class="active"> {{__('website.editprofile')}}</a></li>
                 </ul>
            </div>
        </div>
    </section><!--header-profile-->
    
    <section class="page-profile">
        {!! Form::model($user, ['route' => ['exampublisher.update', $user->id], 'method' => 'patch','files'=>'true']) !!}
            {{ csrf_field() }}
	 		<div class="container">
				<div class="row">
					<div class="col-8  mr-auto ml-auto border-container">
                        @include('flash::message')
                        <h3 class="title-section">{{__('website.editprofile')}}</h3>
                        <p>{{__('website.editprofilep')}}</p>
							<div class="form-group">
								  <label name="profile_image_label">{{__('website.profileimage')}}</label>
								  <input type="file" placeholder="" class="form-control" name="profile"/>
							</div>
							<div class="form-group">
								  <label name="cover_image_label">{{__('website.coverimage')}}</label>
								  <input type="file" placeholder="" class="form-control" name="cover"/>
							</div>
                            <div class="form-group">
                                <label for="firstname" name="name_label">{{__('website.name')}}</label>
                                <input type="text" name="name" class="form-control" id="firstname" placeholder="{{__('website.name')}}" value="{{$user->name}}">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div><!--form-group-->
							<div class="form-group">
                                <label for="myjob" name="my_job_label">{{__('website.myjob')}}</label>
                                <input type="text" name="myjob" class="form-control" id="myjob" placeholder="" value="{{$user->exampublisher->myjob}}">
                            </div><!--form-group-->
                            <div class="form-group">
                                <label for="email" name="email_address_label">{{__('website.Emailadd')}}</label>
                                <input type="text" name="email" class="form-control" id="email" placeholder="" value="{{$user->email}}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div><!--form-group-->
							<div class="form-group">
                                <label for="phone" name="phone_label">{{__('website.Phone')}}</label>
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="" value="{{$user->exampublisher->phone}}">
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong style="color:red;">{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div><!--form-group-->
							<div class="form-group">
                                <label for="my-website" name="my_website_label">{{__('website.Website')}}</label>
                                <input type="text" name="website" class="form-control" id="my-website" placeholder="" value="{{$user->exampublisher->website}}">
                            </div><!--form-group-->
                            <div class="form-group">
                                <label name="company_name_label">{{__('website.Company')}}</label>
                                <input type="text" name="company_name" placeholder="" class="form-control" value="{{$user->exampublisher->company_name}}"/>
                                @if ($errors->has('company_name'))
                                        <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('company_name') }}</strong>
                                        </span>
                                    @endif
                            </div><!--form-group-->
                            <div class="form-group">
                                <label name="company_industry">{{__('website.CompanyIndustry')}}</label>
                                <input type="text" name="company_industry" placeholder="" class="form-control" value="{{$user->exampublisher->company_industry}}"/>
                                {{-- <select class="form-control" name="">
                                    <option selected disabled>Please Select Minimum 3 industries</option>
                                    <option>option</option>
                                    <option>option</option>
                                    <option>option</option>
                                </select>        --}}
                                @if ($errors->has('company_industry'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('company_industry') }}</strong>
                                    </span>
                                @endif                     
                            </div><!--form-group-->
                            <div class="form-group">
                                <label name="password_label" for="password">{{__('website.Password')}}</label>
                                <input type="text" name="password" class="form-control" id="password" placeholder="Enter Your password">
                            </div><!--form-group-->
                            <div class="form-group">
                                <label name="confirm_passward_label" for="re-password">{{__('website.PasswordConfirm')}}</label>
                                <input type="text" name="password_confirmation" class="form-control" id="re-password" placeholder="Retype your password">
                            </div><!--form-group-->
							<div class="form-group">
                                <label for="re-password" name="my_bio_label">{{__('website.bio')}}</label>
								<textarea class="form-control" name="pio">{{$user->exampublisher->pio}}</textarea>
										  
                            </div><!--form-group-->
                            {{-- <div class="form-group">
                                <label for="pr-lang">Your Profile Language</label>
                                <select class="form-control" id="pr-lang" name="profile_language">
                                    <option value="English">English</option>
                                    <option value="Arabic">Arabic</option>
                                </select>
                            </div><!--form-group--> --}}
                            <div class="form-group text-right">
                                <button class="button save-button" name="save_btn">{{__('website.Save')}}</button>
                                <a href="/profile" class="button cancle-button">{{__('website.Cancle')}}</a>
                            </div><!--form-group-->
                    </div>
				</div><!--row-->
			</div><!--container-->
		</form>
	</section>

@endsection