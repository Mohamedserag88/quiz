@extends('layouts.app')

@section('content')
<section class="page-profile">
	
        <div class="container">
            <div class="row">
                    
                <div class="col-md-3">
                        
                    <div class="puplisher-image-profile">
                        <img name="profile_img" src="{{asset(($user->profile)?$user->profile:'images/defultimg.png')}}" class="now-img" alt="" style="height: 100%;"/>	
                        
                    </div><!--puplisher-image-profile-->
                    <div class="puplisher-name">
                        <h4 name="name_h4">{{$user->name}}</h4>
                        <p name="job_p">{{$user->myjob}}</p>
                    </div><!--puplisher-name-->
                    <div class="action-area">
                            {{-- <a href="/followings" class="following"><i class="fas fa-rss"></i> following </a> --}}
                    </div>
                    <div class="samary-area">
                        <ul>
                            <li name="user_website_li"><i class="fas fa-globe"></i> <a href="{{$user->exampublisher->website}}">{{$user->exampublisher->website}}</a></li>
                            <li name="user_phone_li"><i class="fas fa-phone"></i>{{($user->exampublisher)?$user->exampublisher->phone:''}}</li>
                            <li name="user_email_li"><i class="far fa-envelope"></i>{{$user->email}}</li>
                        </ul>
                    </div>
                </div><!--col-->
                <div class="col-md-9 p-0">
                        @include('flash::message')
                    <div class="cover-profile">
                        <img name="cover_img" width="100%" src="{{asset(($user->cover)?$user->cover:'images/defaultcover.gif')}}" alt="" style="height: 100%;"/>
                    </div><!--cover-profile-->
                    <div class="info-puplisher">
                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li name="following_li"> <span><i class="fas fa-rss"></i> {{ count($user->followers) }} {{ __('website.Followers') }}</span></li>
                                    <li name="exam_count_li"><span><i class="far fa-newspaper"></i>  {{$user->examposted->count()}}  {{ __('website.ExamsPublished') }}</span></li>
                                </ul>
                            </div><!--col-->
                            <div class="col-md-6">
                                <ul class="link-prof"> 
                                    <li><a name="exam_create_a" href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/create">{{ __('website.createexam') }}</a></li>
                                    <li><a name="profile_edit_a" href="/{{ LaravelLocalization::getCurrentLocale() }}/profile/edit">{{ __('website.editprofile') }}</a></li>
                                </ul>
                            </div><!--col-->
                        </div><!--row-->
                        
                        
                    </div><!--info-puplisher-->
                    <div class="row m-0">
                        <div class="about-illust" >
                            <h3 name="bio_h3">{{ __('website.bio') }}</h3>
                            <article name="bio_artical">{{$user->exampublisher->pio}}
                            </article>
                        </div><!--about-illust-->
                        <div class="border-bottom"></div>
                        
                        <div class="exams-lecturer trend-exam">
                                
                                <div class="row">
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                            <div class="row m-0">
                                                @foreach($user->examposted as $exam)
                                                    <div class="col-md-4">
                                                        <div class="trend-course">
                                                            <div class="course-img">
                                                                {{-- <a href=""><img src="/images/c1.png" alt=""/></a> --}}
                                                            </div>
                                                            <div class="course-content">
                                                                <h3 name="exam_name_h3"><a href="">{{$exam->name}}</a></h3>
                                                                <p name="exam_last_update_p" class="date"><i class="far fa-clock"></i> {{ __('website.Lastupdated') }} {{$exam->updated_at->format('d/m/Y')}}</p>
                                                                <div class="exampublisher-num">
                                                                    <i class="far fa-user"></i> <span>{{$exam->total_points}} {{ __('website.Points') }}</span>
                                                                </div>
                                                                <div class="rate">
                                                                    @for($i=0;$i<round($exam->rateavg); $i++)
                                                                        <i class="fa fa-star active"></i>
                                                                    @endfor
                                                                    @for($i=0;$i<(5 -round( $exam->rateavg)); $i++)
                                                                        <i class="fa fa-star"></i>
                                                                    @endfor
                                                                    <span>{{ $exam->rateavg }} ({{ count($exam->rates) }} {{ __('website.ratings') }})</span>
                                                                </div>
                                                                {!! Form::open(['route' => ['exams.destroy', $exam->id], 'method' => 'delete']) !!}
                                                                        <a name="exam_edit_a" href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/{{ $exam->code }}/edit" class="exam-edit"><i class="far fa-edit"></i> {{ __('website.edit') }} </a>
                                                                        @if($exam->draft_public)
                                                                            <a href="" class="btn btn-danger btn-xs" id="sharecodemodel" data-toggle="modal" data-target="#sharmodal" exam-code={{ $exam->code }}  class="button">{{ __('website.share') }}</a>
                                                                        @else
                                                                            <a class="btn btn-danger btn-xs" href="/{{ LaravelLocalization::getCurrentLocale() }}/publishexam/{{ $exam->code }}">{{ __('website.publish') }}</a>
                                                                        @endif
                                                                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>'.__('website.delete'), ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','style'=>"float:  right;", 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                                {!! Form::close() !!}
                                                                

                                                            </div>
                                                        </div>
                                                    </div><!--col-->
                                                @endforeach
                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <div class="row m-0">
                                                <div class="col-md-4">
                                                    <div class="trend-course">
                                                        <div class="course-img">
                                                            <a href=""><img src="{{ asset('images/c3.png') }}" alt=""/></a>
                                                        </div>
                                                        <div class="course-content">
                                                            <h3><a href="">Earn Your CSS Certification</a></h3>
                                                            
                                                                

                                                            <a href="#"></a>
                                                            <a href="#" class="exam-edit"><i class="far fa-edit"></i> Edit </a>
                                                        </div>
                                                    </div>
                                                </div><!--col-->
                                            </div><!--row-->
                                        </div><!--tab-pane-->

                                    </div>

                                </div><!--row-->
                            
                            
                    
                        </div><!--exams-lecturer-->
                    </div><!--row-->
                </div><!--col-->
            </div><!--row-->
        </div><!--container-->	

        <!-- Modal -->
    <div class="modal fade" id="sharmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="width: 700px;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ __('website.codes') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="sharmodelbody">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('website.close') }}</button>
                    </div>
                </div>
            </div>
    </div>
	
</section>

@endsection