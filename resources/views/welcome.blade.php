<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quiziu | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style.min.css') }}">
 </head>
<body>
  
	<section class="search-home">
		<header>
			<div class="container">
				<div class="row">
					<div class="col p-0">
                         <div class="logo">
                             <a href="{{ URL::to('/') }}" title="Home"><img src="{{ asset('images/logo-home.png') }}" alt="" /></a>
                         </div>
                    </div>
					<div class="col-lg-4 col-12 menu-top">
                         <ul class="before-login"> 
                            <li>
                                <form action="#">
                                    <select>
                                        <option>English</option>
										<option>العربية</option>
                                    </select>
                                </form>
                            </li>
                            <li><a href="login.html">LOGIN</a></li>
                            <li><a href="SignUp.html">Sign UP</a></li>
                        </ul>
                   </div>
				</div>
			</div><!--container-->
		</header>
		<div class="container">
			<div class="row">
				<form action="#">
					<h4>The Online Exams is at your fingertips.</h4>
					<p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur</p>
					<div class="form-container">
						<input type="text" placeholder="Exam publishers , Exames , Courses" class="form-control"/>
					    <button>Search</button>
					</div>
 					<div class="action-buttons">
                          <a href="#" class="bec-publisher">become a publisher</a>
                        
						  <a href="#" class="start-exam">start the exam</a>
                        
                     </div>
 				</form>
			</div><!--row-->
		</div>	
	</section>
    <main class="main-home">
        <section class="publisher">
            <div class="container">
                <h2>Browse top exam publishers</h2>
                <div class="top-publisher">
                    <h3>Browse top exam publishers</h3>
                    <div class="row">
                        <div class="col-4 col-sm-2">
                            <div class="publisher-item">
                                <img src="{{ asset('images/x1.png') }}" alt=""/>
                                <p>Andrew Michel</p>
                                <p class="colored-p">fashion designer</p>
                                <a href="profile-exam-publishers.html"></a>
                            </div>
                        </div>
                        <div class="col-4 col-sm-2">
                            <div class="publisher-item">
                                <img src="/images/x2.png" alt=""/>
                                <p>Andrew Michel</p>
                                <p class="colored-p">fashion designer</p>
                                <a href="profile-exam-publishers.html"></a>
                            </div>
                        </div>
                        <div class="col-4 col-sm-2">
                            <div class="publisher-item">
                                <img src="/images/x3.png" alt="">
                                <p>Andrew Michel</p>
                                <p class="colored-p">fashion designer</p>
                                <a href="profile-exam-publishers.html"></a>
                            </div>
                        </div>
                        <div class="col-4 col-sm-2">
                            <div class="publisher-item">
                                <img src="/images/x4.png" alt=""/>
                                <p>Andrew Michel</p>
                                <p class="colored-p">fashion designer</p>
                                <a href="profile-exam-publishers.html"></a>
                            </div>
                        </div>
                        <div class="col-4 col-sm-2">
                            <div class="publisher-item">
                                <img src="/images/x5.png" alt="">
                                <p>Andrew Michel</p>
                                <p class="colored-p">fashion designer</p>
                                <a href="profile-exam-publishers.html"></a>
                            </div>
                        </div>
                        <div class="col-4 col-sm-2">
                            <div class="publisher-item">
                                <img src="/images/x6.png" alt="">
                                <p>Andrew Michel</p>
                                <p class="colored-p">fashion designer</p>
                                <a href="profile-exam-publishers.html"></a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="top-exam">
                    <h3>Browse top Quizzes in category</h3>
                    <div class="row">
                        <div class="col-6 col-sm-3">
                            <a href="category.html">graphic design</a>
                            <a href="category.html">business</a>
                            <a href="category.html">IT & software</a>
                            <a href="category.html">personal development</a>
                        </div>
                        <div class="col-6 col-sm-3">
                            <a href="category.html">life style</a>
                            <a href="category.html">Leadership</a>
                            <a href="category.html">marketing online</a>
                            <a href="category.html">mobile developmet</a>
                        </div>
                        <div class="col-6 col-sm-3">
                            <a href="category.html">cooking</a>
                            <a href="category.html">sales</a>
                            <a href="category.html">logo design</a>
                            <a href="category.html">ux design</a>
                        </div>
                        <div class="col-6 col-sm-3">
                            <a href="category.html">articles</a>
                            <a href="category.html">photography</a>
                            <a href="category.html">web design</a>
                            <a href="category.html">web developmet</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--publish-video-->
        <section class="publish-video">
            <img src="/images/publich-video.png" alt="" />
            <div class="video-content">
              <div class="container">
                  <div class="row">
                      <div class="col">
                          <h2>Publish your own  examinations now</h2>
                          <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
                          <div class="add-quiz-button">
                              <button><a href="#">start Now</a></button>
                          </div>
                      </div>
                      <div class="col icon">
                          <a href="https://youtu.be/RsB2yfYEEWs" data-fancybox data-caption="My caption">
                              <i class="fa fa-play-circle"></i>
                          </a>
                      </div>
                  </div>
              </div>
            </div>
        </section>
        <!--Trend-exam-->
        <section class="trend-exam">
            <div class="container">
                <h2>trending examinations</h2>
                <h4>It is a long established fact that a reader will be distracted by the readable contentof a page when looking at its layout</h4>
      			<div class="row">
			  	   
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c1.png" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c2.png" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c3.png" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c4.png" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c5.jpg" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c6.jpg" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c7.jpg" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
					<div class="col-md-3">
                         <div class="trend-course">
                              <div class="course-img">
										<a href="#"><img src="/images/c8.jpg" alt=""></a>
                                    </div>
                              <div class="course-content">
										<h3><a href="#">Earn Your CSS Certification</a></h3>
										<p class="date"><i class="far fa-clock"></i> Last updated 7/2017</p>
										<p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="profile-exam-publishers.html">Ahmed Samir</a></p>
										<div class="student-num">
											<i class="far fa-user"></i> <span>150 student</span>
										</div>
                                             <div class="rate">
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star active"></i>
												<i class="fa fa-star"></i>
												<span>4.6 (28 ratings)</span>
											</div>
                                    </div>
                         </div><!--trend-course-->
                      </div><!--col-->
					
				</div><!--row-->
            </div>

        </section>
        <!--testimonials-->
        <section class="testimonials">
           <div class="container">
               <h2>testimonials</h2>
               <div class="owl-carousel owl-theme" id="owl-example2">
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
                       </div>
                   </div>
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
                       </div>
                   </div>
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
                       </div>
                   </div>
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
                       </div>
                   </div>
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.</p>
                       </div>
                   </div>
               </div>
           </div>
        </section>
        <!--Subscribe-->
        <section class="subscribe">
            <div class="container">
                <h2>Newsletters</h2>
                <h4>Keep me up to date with content and updates</h4>
                <div class="exam-code">
                    <div class="exam-co">
                        <input type="text" class="form-control" placeholder="Email Address">
                        <button type="button">Subscribe</button>
                    </div>
                </div>
            </div>

        </section>
    </main>
    <!--statistics-->
    <section class="statistics">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col">
                    <p><span>2,342,233</span> COMMUNITY MEMBERS</p>
                </div><!--col-->
                <div class="col-md-6 col">
                    <p><span>15,342,216</span> TOTAL quiz</p>
                </div><!--col-->
            </div><!--row-->
        </div><!--container-->
    </section>
    <!--footer-->
  <footer>
        <div class="container">
            <div class="row">
                <div class="top-footer">
                    <ul>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms and Conditions</a></li>
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Quiziu Licenses</a></li>
                        <li><a href="#">Partners</a></li>
                    </ul>
                </div><!--top-footer-->
                <div class="bottom-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-12 copy-right p-0">

                                <p>© 2018 Quiziu - All Rights Reserved.</p>

                            </div>
                            <div class="col-md-6 col-12 social p-0 ">

                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>

                            </div>
                        </div><!--row-->
                    </div><!--container-->
                </div><!--bottom-footer-->
            </div><!--row-->
        </div><!--container-->
    </footer>
    <!--JsFiles-->
    <!--Carousel-->
    <script src="/js/jquery-3.2.1.min.js"></script><!--Jquery-->
    <script src="/js/owl.carousel.min.js"></script><!--Slider-->
    <script src="/js/carousel-script.js"></script><!--Slider-->
    <!--->
    <script src="/js/scripts.min.js"></script><!--Slider-->
    <!--fancy script-->
    <script src="/js/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="/js/facncy-script.js"></script>
</body>
</html>