<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quiziu | Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/style.min.css') }}">
 </head>
<body>
  
	<section class="search-home">
		<header>
			<div class="container">
				<div class="row">
					<div class="col p-0">
                         <div class="logo">
                             <a href="/" title="Home"><img src="/images/logo-home.png" alt="" /></a>
                         </div>
                    </div>
					<div class="col-lg-4 col-12 menu-top">
                         <ul class="before-login"> 
                            @if( LaravelLocalization::getCurrentLocale() == 'en')
                                <li>
                                    <a rel="alternate" hreflang="ar" href="{{ LaravelLocalization::getLocalizedURL('ar', null, [], true) }}">
                                        العربيه 
                                    </a>
                                </li>  
                            @else
                                <li>
                                    <a rel="alternate" hreflang="en" href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">
                                        English 
                                    </a>
                                </li>
                            @endif 
                            {{-- <li>
                                <form action="#">
                                    <select id="sitelanguage">
                                        <option selected disabled>{{ __('website.Site_Language') }}</option>                             
                                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                            <option value="en" {{(App::isLocale('en'))?'selected':''}}><a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">{{(App::isLocale('ar'))?'العربيه':$properties['native']}} </a></option>
                                        @endforeach
                                    </select>
                                </form>
                            </li> --}}
                            @if(\Auth::check())
                                <li>
                                    <div class="user-area" >
                                        <div class="dropdown">
                                            <button class="btn dropdown-toggle" type="button" id="user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:black;">
                                                <div class="img" style="color: white;"><span>{{ \Auth::user()->name[0] }}</span></div>
                                                <div class="name-user" style="color: white;">{{ \Auth::user()->name }}</div>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="user-menu" style="color:black;">
                                                    @if(\Auth::user()->type=='exampublisher')
                                                    <a class="dropdown-item" style="color: black;" href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/create">{{ __('website.createexam') }}</a>
                                                    @else
                                                        <a class="dropdown-item" style="color: black;" href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/codepage">{{ __('website.exam') }}</a>                                    
                                                    @endif
                                                    <a class="dropdown-item" style="color: black;" href="/{{ LaravelLocalization::getCurrentLocale() }}/profile">{{ __('website.profile') }}</a>
                                                    <a class="dropdown-item" style="color: black;" href="/{{ LaravelLocalization::getCurrentLocale() }}/profile/edit">{{ __('website.editprofile') }}</a>
                                                <a class="dropdown-item" style="color: black;" href="/{{ LaravelLocalization::getCurrentLocale() }}/logout">{{ __('website.logout') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @else
                                <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/login">{{ __('website.login') }}</a></li>
                                <li><a href="/{{ LaravelLocalization::getCurrentLocale() }}/register">{{ __('website.signup') }}</a></li>
                            @endif
                        </ul>
                   </div>
				</div>
			</div><!--container-->
		</header>
		<div class="container">
			<div class="row">
                    
                <form method="GET" action="{{ url(LaravelLocalization::getCurrentLocale().'/search') }}">
                    @include('flash::message')
					<h4>{{ __('website.onlineexam') }}</h4>
					<div class="form-container">
						<input type="text" name="search" placeholder="{{ __('website.examplaceholder') }}" class="form-control"/>
                        <button>{{ __('website.Search') }}</button>
                    
					</div>
 					<div class="action-buttons">
                          {{-- <a href="#" class="bec-publisher">become a publisher</a> --}}
                        
						  <a href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/codepage" class="start-exam">{{ __('website.stratexam') }}</a>
                        
                     </div>
 				</form>
			</div><!--row-->
		</div>	
	</section>
    <main class="main-home">
        <section class="publisher">
            <div class="container">
                <h2>{{ __('website.toppublisher') }}</h2>
                <div class="top-publisher">
                    <h3>{{ __('website.browsepublisher') }}</h3>
                    <div class="row">
                        @foreach ($toppublisher as $publisher)
                            <div class="col-4 col-sm-2">
                                <div class="publisher-item">
                                    <img src="{{ asset(($publisher->user->profile)?$publisher->user->profile:'/images/defultimg.png') }}" alt=""/>
                                    <p>{{ $publisher->user->name }}</p>
                                    <p class="colored-p">{{ $publisher->myjob }}</p>
                                    <a href="/users/{{ $publisher->user->id }}"></a>
                                </div>
                            </div>
                        @endforeach
                        
                        

                    </div>
                </div>
                <div class="top-exam">
                    <h3>{{ __('website.topcat') }}</h3>
                    <div class="row">
                        @foreach ($topcategories as $topcategory)
                            <div class="col-6 col-sm-3">
                                @foreach ($topcategory as $category)
                                    <a href="/category/{{$category->slug}}">{{ $category->name }}</a>
                                @endforeach
                                
                            </div>
                        @endforeach
                        
                        
                    </div>
                </div>
            </div>
        </section>
        <!--publish-video-->
        <section class="publish-video">
            <img src="{{ asset('images/publich-video.png') }}" alt="" />
            <div class="video-content">
              <div class="container">
                  <div class="row">
                      <div class="col">
                          <h2>{{ __('website.publish_examinations') }}</h2>
                          <p>{{ __('website.home_text') }}</p>
                          <div class="add-quiz-button">
                              <button><a href="#">{{ __('website.start_Now') }}</a></button>
                          </div>
                      </div>
                      <div class="col icon">
                          <a href="https://youtu.be/RsB2yfYEEWs" data-fancybox data-caption="My caption">
                              <i class="fa fa-play-circle"></i>
                          </a>
                      </div>
                  </div>
              </div>
            </div>
        </section>
        <!--Trend-exam-->
        <section class="trend-exam">
            <div class="container">
                <h2>{{ __('website.homeexam') }}</h2>
                <h4>{{ __('website.homeexam_text') }}</h4>
      			<div class="row">
                    @foreach ($exams as $exam)
                        <div class="col-md-3">
                            <div class="trend-course">
                                <div class="course-img">
                                    <a href="#"><img src="{{ asset($exam->category->image) }}" alt=""></a>
                                </div>
                                <div class="course-content">
                                    <h3><a href="/{{ LaravelLocalization::getCurrentLocale() }}/exams/conditions/{{ $exam->id }}">{{ $exam->name }}</a></h3>
                                    <p class="date"><i class="far fa-clock"></i> Last updated {{ $exam->updated_at->format('d/m/Y') }}</p>
                                <p class="instructor"><i class="fas fa-user-tie"></i> By: <a href="/users/{{ $exam->user->id }}">{{ $exam->user->name }}</a></p>
                                    <div class="student-num">
                                        <i class="far fa-user"></i> <span>{{ $exam->number_of_students }} student</span>
                                    </div>
                                    <div class="rate">
                                        @for($i=0;$i<round($exam->rateavg); $i++)
                                            <i class="fa fa-star active"></i>
                                        @endfor
                                        @for($i=0;$i<(5 -round( $exam->rateavg)); $i++)
                                            <i class="fa fa-star"></i>
                                        @endfor
                                        <span>{{ $exam->rateavg }} ({{ count($exam->rates) }} ratings)</span>
                                    </div>
                                </div>
                            </div><!--trend-course-->
                        </div><!--col-->
                    @endforeach
					
				</div><!--row-->
            </div>

        </section>
        <!--testimonials-->
        <section class="testimonials">
           <div class="container">
               <h2>{{ __('website.testimonials') }}</h2>
               <div class="owl-carousel owl-theme" id="owl-example2">
                    @foreach ($toppublisher as $publisher)
                        <!--item-->
                        <div class="item">
                            <div class="container-item">
                                <p>{{ $publisher->pio }}</p>
                            </div>
                        </div>
                        @endforeach
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>{{ __('website.home_text') }}</p>
                       </div>
                   </div>
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>{{ __('website.home_text') }}</p>
                       </div>
                   </div>
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>{{ __('website.home_text') }}</p>
                       </div>
                   </div>
                   <!--item-->
                   <div class="item">
                       <div class="container-item">
                           <p>{{ __('website.home_text') }}</p>
                       </div>
                   </div>
               </div>
           </div>
        </section>
        <!--Subscribe-->
        <section class="subscribe">
            <div class="container">
                <h2>{{ __('website.Newsletters') }}</h2>
                <h4>{{ __('website.Keep_up_date') }}</h4>
                <div class="exam-code">
                    <div class="exam-co">
                        <input type="text" class="form-control" placeholder="{{ __('website.Emailadd') }}">
                        <button type="button">{{ __('website.Subscribe') }}</button>
                    </div>
                </div>
            </div>

        </section>
    </main>
    <!--statistics-->
    <section class="statistics">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col">
                    <p><span>{{ \App\User::all()->count() }}</span> {{ __('website.footeruser') }}</p>
                </div><!--col-->
                <div class="col-md-6 col">
                    <p><span>{{ \App\Models\Exam::all()->count() }}</span> {{ __('website.footerexam') }}</p>
                </div><!--col-->
            </div><!--row-->
        </div><!--container-->
    </section>
    <!--footer-->
  <footer>
        <div class="container">
            <div class="row">
                <div class="top-footer">
                    <ul>
                        <li><a href="#">{{ __('website.Privacy_Policy') }}</a></li>
                        <li><a href="#">{{ __('website.Terms_and_Conditions') }}</a></li>
                        <li><a href="#">{{ __('website.Help') }}</a></li>
                        <li><a href="#">{{ __('website.Quiziu_Licenses') }}</a></li>
                        <li><a href="#">{{ __('website.Partners') }}</a></li>
                    </ul>
                </div><!--top-footer-->
                <div class="bottom-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-12 copy-right p-0">

                                <p>{{ __('website.Rights_Reserved') }}</p>

                            </div>
                            <div class="col-md-6 col-12 social p-0 ">

                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>

                            </div>
                        </div><!--row-->
                    </div><!--container-->
                </div><!--bottom-footer-->
            </div><!--row-->
        </div><!--container-->
    </footer>
    <!--JsFiles-->
    <!--Carousel-->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script><!--Jquery-->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script><!--Slider-->
    <script src="{{ asset('js/carousel-script.js') }}"></script><!--Slider-->
    <!--->
    <script src="/js/scripts.min.js"></script><!--Slider-->
    <!--fancy script-->
    <script src="{{ asset('js/jquery.fancybox.min.j') }}s"></script>
    <script type="text/javascript" src="{{ asset('js/facncy-script.js') }}"></script>
</body>
</html>