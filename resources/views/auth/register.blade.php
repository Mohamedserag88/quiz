@extends('layouts.app')

@section('content')
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-10  m-auto">

                <div class="sign-head">
                    <h1 >{{ __('website.signup') }}</h1>
                    <div class="row">
                        <div class="col sign-option active" id="student">
                            <h3><a href="#" >{{ __('website.student') }}</a></h3>
                        </div>
                        <div class="col sign-option " id="exampublisher">
                           <h3><a href="#"  >{{ __('website.ExamPublisher') }}</a></h3>
                        </div>
                        
                    </div>
                </div>
                <div class="signUp" id="exampublishertab" style="display:none;">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>{{ __('website.fullname') }}</label>
                            <input type="text" name="name" placeholder="{{ __('website.fullname') }} " value="{{ old('name')?? '' }}" class="form-control"/>
                            @if ($errors->has('name'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('website.Emailadd') }}</label>
                            <input type="text" name="email" placeholder="{{ __('website.Emailadd') }}" value="{{ old('email')?? '' }}" class="form-control"/>
                            @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div><!--form-group-->

                        <div class="form-group">
                            <label>{{ __('website.Company') }}</label>
                            <input type="text" name="company_name" value="{{ old('company_name')?? '' }}" placeholder="{{ __('website.Company') }}" class="form-control"/>
                            @if ($errors->has('company_name'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                        </div><!--form-group-->
                        <div class="form-group">
                            <label>{{ __('website.CompanyIndustry') }}</label>
                            <input type="text" name="company_industry" value="{{ old('company_industry')?? '' }}" placeholder="{{ __('website.CompanyIndustry') }}" class="form-control"/>
                            {{-- <select class="form-control" name="">
                                <option selected disabled>Please Select Minimum 3 industries</option>
                                <option>option</option>
                                <option>option</option>
                                <option>option</option>
                            </select>        --}}
                            @if ($errors->has('company_industry'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('company_industry') }}</strong>
                                </span>
                            @endif                     
                        </div><!--form-group-->

                        <div class="form-group">
                            <label>{{ __('website.Password') }}</label>
                            <input type="password" name="password"  placeholder="{{ __('website.Password') }}" class="form-control" />
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->

                        <div class="form-group">
                            <label>{{ __('website.PasswordConfirm') }}</label>
                            <input type="password" name="password_confirmation" placeholder="{{ __('website.PasswordConfirm') }}" class="form-control" />
                        </div><!--form-group-->

                        <div class="form-group">
                            <input type="radio" class="custom-checkbox" id="remember" name="policyaccept"/>
                            <span>{{ __('website.haveread') }} <span class="term">{{ __('website.Privacy') }}</span> {{ __('website.and') }} <span class="term">{{ __('website.Terms') }}</span></span>
                            @if ($errors->has('policyaccept'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('policyaccept') }}</strong>
                                    </span>
                                @endif
                        </div><!--form-group-->

                        <div class="form-group">
                            <button class="button">{{ __('website.signup') }}</button>
                        </div>
                        <hr>
                    </form>
                    <div class="login">
                        <p class="mr-b-50px">{{ __('website.signupmethode') }}</p>
                        <a href="{{ url('/auth/facebook') }}" class="login-social facebook"><i class="fa fa-facebook"></i>{{ __('website.wfacebook') }}</a>
                        <a href="{{ url('/auth/twitter') }}" class="login-social twitter"><i class="fa fa-twitter"></i> {{ __('website.wtwitter') }}</a>
                        <a href="{{ url('/auth/google') }}" class="login-social google-plus"><i class="fa fa-google-plus"></i> {{ __('website.wgoogle') }}</a>
                        <a href="{{ url('/auth/linkedin') }}" class="login-social linkedin"><i class="fa fa-linkedin"></i>{{ __('website.wlinckedin') }}</a>
                    </div>
                </div>
                <div class="signUp" id="studenttab" >
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>{{ __('website.Firstname') }}</label>
                            <input type="text" name="first_name" value="{{ old('first_name')?? '' }}" placeholder="{{ __('website.Firstname') }}" class="form-control" required/>
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('website.lastname') }}</label>
                            <input type="text" name="last_name" value="{{ old('last_name')?? '' }}" placeholder="{{ __('website.lastname') }}" class="form-control" required/>
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('website.Emailadd') }}</label>
                            <input type="text" name="email" value="{{ old('email')?? '' }}" placeholder="{{ __('website.Emailadd') }} " class="form-control" required/>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->
                        <div class="form-group">
                            <label>{{ __('website.Password') }}</label>
                            <input type="password" name="password" placeholder="{{ __('website.Password') }}" class="form-control" required/>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->

                        <div class="form-group">
                            <label>{{ __('website.PasswordConfirm') }}</label>
                            <input type="password" name="password_confirmation" placeholder="{{ __('website.PasswordConfirm') }}" class="form-control" required/>
                        </div><!--form-group-->

                        <div class="form-group">
                            <input type="radio" class="custom-checkbox" id="remember" name="policyaccept" required/>
                            <span>{{ __('website.haveread') }} <span class="term">{{ __('website.Privacy') }}</span> {{ __('website.and') }} <span class="term">{{ __('website.Terms') }}</span></span>
                            
                            @if ($errors->has('policyaccept'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('policyaccept') }}</strong>
                                </span>
                            @endif
                        </div><!--form-group-->

                        <div class="form-group">
                            <button class="button">{{ __('website.signup') }}</button>
                        </div>
                        <hr>
                    </form>
                    <div class="login">
                        <p class="mr-b-50px">{{ __('website.signupmethode') }}</p>
                        <a href="{{ url('/auth/facebook') }}" class="login-social facebook"><i class="fa fa-facebook"></i>{{ __('website.wfacebook') }}</a>
                        <a href="{{ url('/auth/twitter') }}" class="login-social twitter"><i class="fa fa-twitter"></i> {{ __('website.wtwitter') }}</a>
                        <a href="{{ url('/auth/google') }}" class="login-social google-plus"><i class="fa fa-google-plus"></i> {{ __('website.wgoogle') }}</a>
                        <a href="{{ url('/auth/linkedin') }}" class="login-social linkedin"><i class="fa fa-linkedin"></i>{{ __('website.wlinckedin') }}</a>
                    </div>
                </div>
            </div><!--col-->
        </div>
    </div><!--container-->
</main>


@endsection
