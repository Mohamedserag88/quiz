

@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-10  m-auto login">
                        <form method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <h1>{{ __('website.signin') }}</h1>
                            <p class="mr-b-50px">{{ __('website.loginmethode') }}</p>

                            <a href="/auth/facebook" class="login-social facebook"><i class="fa fa-facebook"></i> {{ __('website.wfacebook') }}</a>
                            <a href="/auth/twitter" class="login-social twitter"><i class="fa fa-twitter"></i>{{ __('website.wtwitter') }}</a>
                            <a href="/auth/google" class="login-social google-plus"><i class="fa fa-google-plus"></i>{{ __('website.wgoogle') }}  </a>
                            <a href="/auth/linkedin" class="login-social linkedin"><i class="fa fa-linkedin"></i>{{ __('website.wlinckedin') }} </a>

                            <p class="mr-t-b-50px">{{ __('website.mailsigen') }}</p>

                            <div class="form-group">
                                <input type="text" placeholder="{{ __('website.useremail') }} " class="form-control" name="email" required/>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div><!--form-group-->

                            <div class="form-group">
                                <input type="password" placeholder="{{ __('website.Password') }}" class="form-control" name="password" required/>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div><!--form-group-->

                            <div class="form-group mr-b-30px">
                                <div class="row no-gutters">
                                    <div class="col p-0">
                                        <input type="checkbox" class="custom-checkbox" id="remember" name="rememberme"/>
                                        <label for="remember"></label>
                                    </div><!--col-->
                                    <div class="col p-0">
                                        <a href="#" class="forget-pass">{{ __('website.forgot') }}</a>
                                    </div><!--col-->
                                </div><!--row-->
                                
                            </div><!--form-group-->

                            <div class="form-group">
                                <button class="button">{{ __('website.login') }}</button>
                            </div>
                            <p class="mr-t-50px">{{ __('website.haveaccount') }} <a href="/{{ LaravelLocalization::getCurrentLocale() }}/register"> {{ __('website.signup') }} </a></p>
                        </form>
                </div><!--col-->
            </div>
        </div><!--container-->
    </main>
    
@endsection
    
