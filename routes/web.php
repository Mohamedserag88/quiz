<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('/setusertype/{type}',function($type){
    \Session::put('type',$type);
});
Route::get('setlanguage/{locale}', function ($locale) {
    App::setLocale($locale);
    LaravelLocalization::setLocale($locale);
    return LaravelLocalization::getCurrentLocale(); //App::getLocale();
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/privacy',function (){
    return response()->download('privacy.pdf');
});
Route::group([
    'prefix' => LaravelLocalization::setLocale()
],
    function() {
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
        Auth::routes();
        Route::get('/','HomeController@index');
        Route::get("search","HomeController@Search");
        Route::get('users/{id}','UserController@show');
        Route::resource('category','CatgroyController');
        Route::get('exams/conditions/{id}','ExamController@conditions');
        Route::get('/terms',function(){
            return view('site.terms');
        });
    });
// Route::get('category/{slug}','CatgroyController@showact');
Route::group([
    'prefix' => LaravelLocalization::setLocale(),'middleware' => ['web','auth'],
],
    function() {
        Route::get('profile','UserController@profile');
        Route::get('profile/edit','UserController@editprofile');
        Route::resource('students','StudentController');
        Route::resource('exampublisher','ExamPublisherController');
        Route::get('exams/codepage','ExamController@codepage');
        Route::post('/exams/correct','ExamController@correct')->name('exams.correct');
        Route::resource('exams','ExamController');
        Route::get('exams/code/{id}','ExamController@code');
        Route::get('exam/pre','ExamController@exampre')->name('exams.precode');
        Route::get('exam/examprepage','ExamController@examprepage')->name('exams.examprepage');
        Route::get('exam/result','ExamController@examresult');
        Route::resource('questions','QuestionController');
        Route::post('/rate','RatingController@rate')->name('rate');
        Route::get('rateForm/','RatingController@rateForm');
        Route::get('follow/{following}/{follower}','UserController@follow');
        Route::resource('codes','CodesController');
        Route::get('publishexam/{code}','ExamController@publishexam');
        Route::get('getexamcodes/{code}','ExamController@getexamcodes');
        Route::post('/sharecode','ExamController@sharecode')->name('sharecode');
        Route::get('/examsolve/{code}','ExamController@examsolve');


});
Route::group(['as' => 'admin.',
    'prefix' => LaravelLocalization::setLocale().'/admin','middleware' => ['web','auth','admin'],
],
    function() {
        Route::get('/','Admin\HomeController@index');
        Route::resource('students','Admin\StudentController');
        Route::resource('exampublisher','Admin\ExamPublisherController');
        Route::resource('exams','Admin\ExamController');
        Route::resource('questions','Admin\QuestionController');
        Route::resource('users','Admin\UserController');
        Route::resource('category','Admin\CatgroyController');
        Route::resource('codes','Admin\CodesController');

});
