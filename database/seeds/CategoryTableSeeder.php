<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();
        \App\Models\Category::create([
            'name'=>'Graphic Design',
            'slug'=>'graphic_design',
            'descrpition'=>'Graphic Design',
            'image'=>'/uploads/GraphicDesign.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Business',
            'slug'=>'business',
            'descrpition'=>'Business',
            'image'=>'/uploads/Business.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'IT & Software',
            'slug'=>'it_software',
            'descrpition'=>'IT & Software',
            'image'=>'/uploads/ITSoftware.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Personal Development',
            'slug'=>'personal_development',
            'descrpition'=>'Personal Development',
            'image'=>'/uploads/personal-development.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Life Style',
            'slug'=>'life_style',
            'descrpition'=>'Life Style',
            'image'=>'/uploads/LifeStyle.png'
        ]);
        \App\Models\Category::create([
            'name'=>'Leadership',
            'slug'=>'leadership',
            'descrpition'=>'Leadership',
            'image'=>'/uploads/Leadership.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Marketing Online',
            'slug'=>'marketing_online',
            'descrpition'=>'Marketing Online',
            'image'=>'/uploads/online-marketing.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Mobile Developmet',
            'slug'=>'mobile_developmet',
            'descrpition'=>'Mobile Developmet',
            'image'=>'/uploads/android-app-development.png'
        ]);
        \App\Models\Category::create([
            'name'=>'Cooking',
            'slug'=>'cooking',
            'descrpition'=>'Cooking',
            'image'=>'/uploads/Cooking.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Sales',
            'slug'=>'sales',
            'descrpition'=>'Sales',
            'image'=>'/uploads/sales.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Logo Design',
            'slug'=>'logo_design',
            'descrpition'=>'Logo Design',
            'image'=>'/uploads/LogoDesign.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Ux Design',
            'slug'=>'ux_design',
            'descrpition'=>'Ux Design',
            'image'=>'/uploads/UxDesign.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Photography',
            'slug'=>'photography',
            'descrpition'=>'Photography',
            'image'=>'/uploads/Photography.jpeg'
        ]);
        \App\Models\Category::create([
            'name'=>'Articles',
            'slug'=>'articles',
            'descrpition'=>'Articles',
            'image'=>'/uploads/Articles.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Web Design',
            'slug'=>'web_design',
            'descrpition'=>'Ux Design',
            'image'=>'/uploads/WebDesign.jpg'
        ]);
        \App\Models\Category::create([
            'name'=>'Web Developmet',
            'slug'=>'web_developmet',
            'descrpition'=>'Web Developmet',
            'image'=>'/uploads/WebDevelopmet.png'
        ]);

    }
}
