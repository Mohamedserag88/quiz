<?php

use Illuminate\Database\Seeder;
use App\Models\Code;

class CodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('codes')->delete();
        Code::create([
            'exam_id'=>1,
            'code'=>'zbpcaalwrvlp8vg4gnwq4',
            'type'=>'Student',
            'student_number'=>1,
            'used'=>0
        ]);
        Code::create([
            'exam_id'=>1,
            'code'=>'7eoqiwhn8jnsp6ev76p29o',
            'type'=>'Student',
            'student_number'=>1,
            'used'=>0
        ]);
        Code::create([
            'exam_id'=>1,
            'code'=>'gksx543y0zjtplw0bfu52',
            'type'=>'Group',
            'student_number'=>20,
            'used'=>0
        ]);
        Code::create([
            'exam_id'=>1,
            'code'=>'w0lzaqiimytpcs5q55y2o',
            'type'=>'Group',
            'student_number'=>15,
            'used'=>0
        ]);
        Code::create([
            'exam_id'=>2,
            'code'=>'xq0a5bksnvoh4y3ds1vx',
            'type'=>'Student',
            'student_number'=>1,
            'used'=>0
        ]);
        Code::create([
            'exam_id'=>2,
            'code'=>'zbpcaalwrvlp8vg4gnwq4',
            'type'=>'Student',
            'student_number'=>1,
            'used'=>0
        ]);
        Code::create([
            'exam_id'=>2,
            'code'=>'7eoqiwhn8jnsp6ev76p29o',
            'type'=>'Group',
            'student_number'=>20,
            'used'=>0
        ]);
        Code::create([
            'exam_id'=>2,
            'code'=>'gksx543y0zjtplw0bfu52',
            'type'=>'Group',
            'student_number'=>15,
            'used'=>0
        ]);
        
        
    }
}