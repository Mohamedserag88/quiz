<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->delete();
        DB::table('students')->delete();
        DB::table('exam_publishers')->delete();
        $user=User::create([
            'name'=>'Exam publisher',
            'email'=>'exampublisher@quiziu.com', 
            'password'=>bcrypt('123456'),
            'type'=>'exampublisher'
        ]);
        $user->exampublisher()->save(factory(App\Models\ExamPublisher::class)->make());
        User::create([
            'name'=>'admin',
            'email'=>'admin@quiziu.com', 
            'password'=>bcrypt('123456'),
            'type'=>'admin'
        ]);
        $user=User::create([
            'name'=>'Exam publisher',
            'email'=>'student@quiziu.com', 
            'password'=>bcrypt('123456'),
            'type'=>'student'
        ]);
        $user->student()->save(factory(App\Models\Student::class)->make());

		factory(App\User::class, 50)->create()->each(function ($u) {
            if($u->type == 'student')
                $u->student()->save(factory(App\Models\Student::class)->make());
            else
                $u->exampublisher()->save(factory(App\Models\ExamPublisher::class)->make());
		});

    }
}
