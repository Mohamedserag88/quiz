<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(TypeTableSeeder::class);
        $this->call(ExamSeeder::class);
        $this->call(CodesSeeder::class);
        $this->call(QuestionsSeeder::class);
        $this->call(AnswerSeeder::class);
    }
}
