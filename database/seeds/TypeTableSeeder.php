<?php

use Illuminate\Database\Seeder;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->truncate();
        \App\Models\Type::create([
            'name'=>'Choose the correct',
            'slug'=>'choose_the_correct',
            'descrpition'=>'Choose the correct'
        ]);
        \App\Models\Type::create([
            'name'=>'Multiple choice',
            'slug'=>'multiple_choice',
            'descrpition'=>'Multiple choice'
        ]);
        \App\Models\Type::create([
            'name'=>'Free Text',
            'slug'=>'free_text',
            'descrpition'=>'Free Text'
        ]);
        \App\Models\Type::create([
            'name'=>'Fill in the blanks',
            'slug'=>'fill_in_the_blanks',
            'descrpition'=>'Fill in the blanks'
        ]);
    }
}
