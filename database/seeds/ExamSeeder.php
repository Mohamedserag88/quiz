<?php

use Illuminate\Database\Seeder;
use App\Models\Exam;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('exams')->delete();
        Exam::create([
            'name'=>'امتحان مادة الرياضيات 1',
            'user_id'=>1,
            'conditions'=>'يرجى الاجابه على جميع الاسئله الموجوده',
            'language'=>'Arabic',
            'category_id'=>1,
            'total_points'=>60,
            'tags_skills'=>'math,reading',
            'code'=>'math20161student',
            'draft_public'=>1,
            'price'=>0,
            'timer'=>60
        ]);
        Exam::create([
            'name'=>'امتحان مادة الرياضيات 2',
            'user_id'=>1,
            'conditions'=>'يرجى الاجابه على جميع الاسئله الموجوده',
            'language'=>'Arabic',
            'category_id'=>1,
            'total_points'=>60,
            'tags_skills'=>'math,reading',
            'code'=>'math20162student',
            'draft_public'=>1,
            'price'=>0,
            'timer'=>60
        ]);
        Exam::create([
            'name'=>'امتحان مادة الرياضيات 3',
            'user_id'=>1,
            'conditions'=>'يرجى الاجابه على جميع الاسئله الموجوده',
            'language'=>'Arabic',
            'category_id'=>1,
            'total_points'=>60,
            'tags_skills'=>'math,reading',
            'code'=>'math20163student',
            'draft_public'=>1,
            'price'=>0,
            'timer'=>60
        ]);
        Exam::create([
            'name'=>'امتحان مادة الرياضيات 4',
            'user_id'=>1,
            'conditions'=>'يرجى الاجابه على جميع الاسئله الموجوده',
            'language'=>'Arabic',
            'category_id'=>1,
            'total_points'=>60,
            'tags_skills'=>'math,reading',
            'code'=>'math20164student',
            'draft_public'=>1,
            'price'=>0,
            'timer'=>60
        ]);
        Exam::create([
            'name'=>'امتحان مادة الرياضيات 5',
            'user_id'=>1,
            'conditions'=>'يرجى الاجابه على جميع الاسئله الموجوده',
            'language'=>'Arabic',
            'category_id'=>1,
            'total_points'=>60,
            'tags_skills'=>'math,reading',
            'code'=>'math20165student',
            'draft_public'=>1,
            'price'=>0,
            'timer'=>60
        ]);
        Exam::create([
            'name'=>'امتحان مادة الرياضيات 6',
            'user_id'=>1,
            'conditions'=>'يرجى الاجابه على جميع الاسئله الموجوده',
            'language'=>'Arabic',
            'category_id'=>1,
            'total_points'=>60,
            'tags_skills'=>'math,reading',
            'code'=>'math20166student',
            'draft_public'=>1,
            'price'=>0,
            'timer'=>60
        ]);
        
        // DB::table('questions')->delete();
		// factory(App\Models\Exam::class, 50)->create()->each(function ($exam) {
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
        //     $exam->questions()->save(factory(App\Models\Question::class)->make());
		// });

    }
}