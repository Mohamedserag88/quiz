<?php

use Illuminate\Database\Seeder;
use App\Models\Question;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('questions')->delete();
        Question::create([
            'type_id'=>1,
            'exam_id'=>1,
            'question'=>'فى الواحد الصحيح عدد ..... ارباع',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>1,
            'exam_id'=>1,
            'question'=>'الشهر التالى لشهر شعبان هو شهر',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>1,
            'exam_id'=>1,
            'question'=>'ثلث ساعه = ... دقيقه',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>1,
            'exam_id'=>1,
            'question'=>'ا كجم = 1\2 كجم +....كجم',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>1,
            'question'=>'اكمل بنفس التسلسل (1,3,5,..) .',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>1,
            'question'=>'محيط الشكل الذى بالصورة =.....وحده',
            'points'=>'3',
            'photo'=>'/uploads/area.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>1,
            'question'=>'ساعه ونصف =....دقيقه',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>1,
            'question'=>'(2,4,6,..) أكمل بنفس التسلسل',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>1,
            'question'=>'أكتب الكسر الذى يمثله الظل ',
            'points'=>'3',
            'photo'=>'/uploads/half.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>1,
            'question'=>'أكتب الكسر الذى يمثله الظل ',
            'points'=>'3',
            'photo'=>'/uploads/3.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>1,
            'question'=>'أكتب الكسر الذى يمثله الظل ',
            'points'=>'3',
            'photo'=>'/uploads/big.PNG'
        ]);
        Question::create([
            'type_id'=>3,
            'exam_id'=>1,
            'question'=>'اشترت سلوى 5 كجم من البرتقال بسعر 6 للكجم ,فكم دفعت سلوى من المال؟',
            'points'=>'8',
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'أوجد حاصل ضرب الأتى :',
            'points'=>'4',
            'photo'=>'/uploads/9x4.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'أوجد حاصل ضرب الأتى :',
            'points'=>'4',
            'photo'=>'/uploads/8x5.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'أوجد حاصل ضرب الأتى :',
            'points'=>'4',
            'photo'=>'/uploads/5x5.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'أوجد حاصل ضرب الأتى :',
            'points'=>'4',
            'photo'=>'/uploads/3x5.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'ضع علامه (<) او علامه (>) اوعلامه (=) :',
            'points'=>'3',
            'photo'=>'/uploads/2x9.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'ضع علامه (<) او علامه (>) اوعلامه (=) :',
            'points'=>'3',
            'photo'=>'/public/uploads/5x8.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'ضع علامه (<) او علامه (>) اوعلامه (=) :',
            'points'=>'3',
            'photo'=>'/uploads/2x4.PNG'
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'ضع علامه (<) او علامه (>) اوعلامه (=) :',
            'points'=>'3',
            'photo'=>'/uploads/2x3.PNG'
        ]);
        Question::create([
            'type_id'=>1,
            'exam_id'=>2,
            'question'=>'ساعه = .... دقيقه',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>1,
            'exam_id'=>2,
            'question'=>'ساعه و 5 دقائق= .... دقيقه',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>1,
            'exam_id'=>2,
            'question'=>'الشهر التالى لشهر مارس ...',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>1,
            'exam_id'=>2,
            'question'=>'الشهر السابق لشهر شعبان',
            'points'=>'3',
        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'أوجد محيط الشكل التالى',
            'points'=>'3',
            'photo'=>'/uploads/trin1.PNG'

        ]);
        Question::create([
            'type_id'=>4,
            'exam_id'=>2,
            'question'=>'أوجد محيط الشكل التالى',
            'points'=>'3',
            'photo'=>'/uploads/squ1.PNG'

        ]);
        Question::create([
            'type_id'=>3,
            'exam_id'=>2,
            'question'=>'قسم شادى 35 جنيها على 5 اطفال بالتساوى احسب نصيب كل طفل',
            'points'=>'3',

        ]);
        
        
        

    }
}