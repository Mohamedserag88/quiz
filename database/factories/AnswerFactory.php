<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Answer::class, function (Faker $faker) {
    return [
        'question_id'=>App\Models\Question::all()->random()->id,
        'answers'=> $faker->sentence(6, true),
        'correct_answer'=>array_rand([0,1]),
        
    ];
});