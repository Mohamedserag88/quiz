<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Question::class, function (Faker $faker) {
    return [
        'type_id'=>App\Models\Type::all()->random()->id,
        'exam_id'=>App\Models\Exam::all()->random()->id,
        'question'=>$faker->sentence(8, true),
        'hint'=>$faker->realtext,
        'points'=>rand (10, 30),
        'photo'=>$faker->imageUrl(640,480)
        
    ];
});