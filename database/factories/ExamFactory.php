<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Exam::class, function (Faker $faker) {
    return [
        'name'=> $faker->sentence(6, true),
        'user_id'=>App\Models\ExamPublisher::all()->random()->id,
        'conditions'=>$faker->realtext,
        'language'=>array_rand(['English'=>'English','Arabic'=>'Arabic','Frensh'=>'Frensh']),
        'category_id'=>App\Models\Category::all()->random()->id,
        'total_points'=>rand (100, 500),
        'tags_skills'=>'html,php,c#,c++,javascript,java',
        'code'=>str_replace("@","",$faker->unique()->safeEmail),
        'draft_public'=>'yes',
        'timer'=>1,
        'price'=>rand (1, 30)
    ];
});