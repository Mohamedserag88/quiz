<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->text('conditions')->nullable();
            $table->string('language');
            $table->integer('total_points');
            $table->text('tags_skills')->nullable();
            $table->string('code');
            $table->string('draft_public')->nullable();
            $table->integer('category_id');
            $table->string('image')->nullable();
            $table->double('price');
            $table->double('timer');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
